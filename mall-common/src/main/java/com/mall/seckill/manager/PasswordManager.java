package com.mall.seckill.manager;

import cn.hutool.crypto.symmetric.AES;
import com.mall.seckill.exception.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

/**
 * @Author macro chen
 * @Date 2022-11-16 09:28:24
 * @Describe 密码管理器
 **/
@Component
public class PasswordManager {

    private static final Logger logger = LoggerFactory.getLogger(PasswordManager.class);

    /**
     * 用于aes签名的key，16 * 8bit = 128bit
     */
    public static String passwordSignKey = "signKey-password";

    private static final AES AES_TARGET = new AES(passwordSignKey.getBytes(StandardCharsets.UTF_8));

    public String decryptPassword(String data) {
        String decryptStr;
        String decryptPassword;
        try {
            decryptStr = AES_TARGET.decryptStr(data);
            decryptPassword = decryptStr.substring(13);
        } catch (Exception e) {
            logger.error("Exception:", e);
            throw new ApiException("密码解密错误", e);
        }
        return decryptPassword;
    }

    /**
     * aes加密 加密方式当前时间戳加上内容
     * @param content 内容
     * @return 密文
     */
    public String encrypt(String content) {
        long timeMillis = System.currentTimeMillis();
        return AES_TARGET.encryptBase64(timeMillis + content);
    }
}
