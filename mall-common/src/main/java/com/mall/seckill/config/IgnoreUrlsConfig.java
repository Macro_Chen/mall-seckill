package com.mall.seckill.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @Title: serviceIgnoreUrlProperties
 * @Author Macro Chen
 * @Package com.mall.seckill.config
 * @Date 2023/1/1 1:33
 * @description: 服务白名单配置
 */

@Data
@EqualsAndHashCode(callSuper = false)
@Configuration
@ConfigurationProperties(prefix="secure.ignore")
public class IgnoreUrlsConfig {
    private List<String> urls;
}
