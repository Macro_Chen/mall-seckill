package com.mall.seckill.config;

import cn.hutool.core.util.ReUtil;
import com.mall.seckill.annotation.Inner;
import com.mall.seckill.constant.CommonConstant;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * @Title: IgnoreUrlProperties
 * @Author Macro Chen
 * @Package com.mall.seckill.init
 * @Date 2022/12/31 16:13
 * @description: 初始化白名单
 */
@Configuration
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "inner", name="enable")
public class IgnoreUrlProperties implements InitializingBean {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final ApplicationContext applicationContext;
    private static final Pattern PATTERN = Pattern.compile("\\{(.*?)\\}");

    @Autowired
    private IgnoreUrlsConfig ignoreUrlsConfig;

    @Override
    public void afterPropertiesSet() throws Exception {
        List<String> urls = new ArrayList<>();
        log.info("加载@Inner接口前白名单数量:{}", ignoreUrlsConfig.getUrls().size());
        RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
        map.keySet().forEach(info -> {
            HandlerMethod handlerMethod = map.get(info);
            // 获取方法上的注解 替代PathVariable 为 *
            Inner method = AnnotationUtils.findAnnotation(handlerMethod.getMethod(), Inner.class);
            Optional.ofNullable(method)
                    .ifPresent(inner -> info.getPatternsCondition().getPatterns()
                            .forEach(url ->  urls.add(ReUtil.replaceAll(url, PATTERN, CommonConstant.ASTERISK))));
            // 获取类上的注解 替代PathVariable 为 *
            Inner controller = AnnotationUtils.findAnnotation(handlerMethod.getBeanType(), Inner.class);
            Optional.ofNullable(controller)
                    .ifPresent(inner -> info.getPatternsCondition().getPatterns()
                            .forEach(url -> urls.add(ReUtil.replaceAll(url, PATTERN, CommonConstant.ASTERISK))));
        });
        urls.addAll(ignoreUrlsConfig.getUrls());
        ignoreUrlsConfig.setUrls(urls);
        log.info("加载后数量白名单:{}", ignoreUrlsConfig.getUrls().size());
    }
}
