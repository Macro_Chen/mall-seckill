package com.mall.seckill.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author 陈述智  梦想开始远航的地方
 * @date 2021/12/15 17:51
 * description 客户端dto
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientDto implements Serializable {

    private Long id;

    @ApiModelProperty(value = "客户端id")
    private String clientId;

    @ApiModelProperty(value = "客户端名称")
    private String clientName;

    @ApiModelProperty(value = "客户端秘钥")
    private String clientSecret;

    @ApiModelProperty(value = "客户端的授权类型")
    private List<String> authorizedGrantTypes;

    @ApiModelProperty(value = "客户端授权码模式回调地址")
    private String redirectUri;

    @ApiModelProperty(value = "accessToken过期时间")
    private Integer accessTokenValidity;

    @ApiModelProperty(value = "刷新token过期时间")
    private Integer refreshTokenValidity;

    @ApiModelProperty(value = "是否自动授权")
    private String autoApprove;

    @ApiModelProperty(value = "权限区域")
    private String scope;

    @ApiModelProperty(value = "附加说明")
    private String additionalInformation;
}
