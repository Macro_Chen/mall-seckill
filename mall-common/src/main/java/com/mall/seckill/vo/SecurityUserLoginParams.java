package com.mall.seckill.vo;

import com.mall.seckill.annotation.EncryptField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @Title: LoginParams
 * @Author Macro Chen
 * @Package com.mall.seckill.domain
 * @Date 2023/1/7 18:49
 * @description: 登录参数
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecurityUserLoginParams {

    /**
     * 用户名
     */
    @NotNull(message = "用户名不能为空")
    private String username;

    /**
     * 密码
     */
    @NotNull(message = "请输入密码")
    @EncryptField
    private String password;

}
