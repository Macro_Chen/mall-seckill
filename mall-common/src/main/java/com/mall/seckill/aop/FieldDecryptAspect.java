package com.mall.seckill.aop;

import com.mall.seckill.annotation.DecryptPoint;
import com.mall.seckill.annotation.EncryptField;
import com.mall.seckill.manager.PasswordManager;
import com.mall.seckill.utils.AseUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Objects;

/**
 * @Title FieldDecryptAspect
 * @Author macroChen
 * @Date 2022-12-12 10:40:47
 * @description 字段解密切面
 * @Version 1.0
 **/
@Aspect
@Component
@Order(2)
public class FieldDecryptAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(FieldDecryptAspect.class);

    @Pointcut("execution(public * com.mall.seckill.controller.*.*.*(..))||execution(public * com.mall.seckill.*.controller.*.*(..))")
    public void decrypt() {
    }

    @Autowired
    private PasswordManager passwordManager;

    @Before("decrypt()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
    }

    @Around("decrypt() && @annotation(decryptPoint)")
    public Object doAfterReturning(ProceedingJoinPoint joinPoint, DecryptPoint decryptPoint) throws Throwable {
        Object responseObj = null;
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            handleDecrypt(arg);
            responseObj = joinPoint.proceed();
//            handleEncrypt(responseObj);
        }
        return responseObj;
    }

    /**
     * 处理加密
     *
     * @param requestObj
     */
    private void handleEncrypt(Object requestObj) throws IllegalAccessException {
        if (Objects.isNull(requestObj)) {
            return;
        }
        Field[] fields = requestObj.getClass().getDeclaredFields();
        for (Field field : fields) {
            boolean hasSecureField = field.isAnnotationPresent(EncryptField.class);
            if (hasSecureField) {
                field.setAccessible(true);
                Object plaintextValue = field.get(requestObj);
                String encryptValue = AseUtil.encrypt(plaintextValue.toString());
                field.set(requestObj, encryptValue);
            }
        }
    }

    /**
     * 处理解密
     */
    private void handleDecrypt(Object responseObj) throws IllegalAccessException {
        if (Objects.isNull(responseObj)) {
            return;
        }

        Field[] fields = responseObj.getClass().getDeclaredFields();
        for (Field field : fields) {
            boolean hasSecureField = field.isAnnotationPresent(EncryptField.class);
            if (hasSecureField) {
                field.setAccessible(true);
                String encryptValue = (String) field.get(responseObj);
                String plaintextValue = passwordManager.decryptPassword(encryptValue);
                System.out.println("decrypt result = " + plaintextValue);
                field.set(responseObj, plaintextValue);
            }
        }
    }
}
