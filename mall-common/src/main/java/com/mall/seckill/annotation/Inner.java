package com.mall.seckill.annotation;

import java.lang.annotation.*;

/**
 * @Title: Inner
 * @Author Macro Chen
 * @Package com.mall.seckill.annotation
 * @Date 2022/12/31 14:58
 * @description: 内部服务调用注解
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Inner {
    /**
     * 是否AOP统一处理(可以理解为是否仅允许Feign之间调用)
     *
     * @return false, true
     */
    boolean value() default true;

    /**
     * 需要特殊判空的字段(预留)
     *
     * @return {}
     */
    String[] field() default {};
}
