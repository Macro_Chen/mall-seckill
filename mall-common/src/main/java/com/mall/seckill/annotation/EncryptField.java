package com.mall.seckill.annotation;

import java.lang.annotation.*;

/**
 * @Title EncryptField
 * @Author macroChen
 * @Date 2022-12-12 15:24:25
 * @description
 * @Version 1.0
 **/
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EncryptField {
}
