package com.mall.seckill.annotation;

import java.lang.annotation.*;

/**
 * @Title DecryptPoint
 * @Author macroChen
 * @Date 2022-12-12 10:38:32
 * @description 字段解密注解
 * @Version 1.0
 **/
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DecryptPoint {

}
