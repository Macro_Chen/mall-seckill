package com.mall.seckill.constant;

/**
 * @Title CommonConstant
 * @Author macroChen
 * @Date 2022-12-16 16:29:37
 * @description
 * @Version 1.0
 **/
public interface CommonConstant {

    String TOO_MANY_VISITORS = "访问人数过多,请稍后尝试";

    String NOT_FOUND_CLIENT = "客户端错误";

    String NOT_FOUND_GRANT_TYPE = "授权类型错误";

    /**
     * 授权页面名称
     */
    String AUTHORIZE_PAGE = "authorization";

    /**
     * 自定义登录页面名称
     */
    String LOGIN_PAGE = "authorization";

    /**
     * 根据角色校验接口资源权限
     */
    String PERMISSION_CHECK_BY_ROLE = "role";

    /**
     * 根据授权作用域校验校验资源权限
     */
    String PERMISSION_CHECK_BY_SCOPE = "scope";

    /**
     * 内部服务调用请求头标识
     */
    String FROM = "from";

    /**
     * 内部服务调用请求头标示值
     */
    String FROM_IN = "from in";
    String ASTERISK = "**";
}
