package com.mall.seckill.constant;

/**
 * @Title: SserviceNameConstant
 * @Author Macro Chen
 * @Package com.mall.seckill.constant
 * @Date 2022/12/24 20:45
 * @description: 服务名常量
 */
public interface ServiceNameConstant {

     String MACRO_ADMIN = "macro-admin";

     String MACRO_AUTH = "macro-auth";
}
