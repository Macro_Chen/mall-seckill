package com.mall.seckill.constant;

/**
 * @Title: NumberConstant
 * @Author Macro Chen
 * @Package com.macro.mall.constant
 * @Date 2022/11/5 19:42
 * @description: 常用数字常量
 */
public class NumberConstant {

    public static final Integer ZERO = 0;

    public static final Integer ONE = 1;

    public static final Integer TWO = 2;

    public static final Integer THREE = 3;

    public static final Integer FOUR = 4;

    public static final Integer FIRE = 5;

    public static final Integer SIX = 6;

    public static final Integer SEVERN = 7;

    public static final Integer EIGHT = 8;

    public static final Integer NINE = 9;

    public static final Integer TEN = 10;

    public static final Integer ONE_HUNDRED = 100;


}
