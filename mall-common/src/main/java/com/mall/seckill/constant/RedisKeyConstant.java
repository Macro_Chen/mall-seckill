package com.mall.seckill.constant;

/**
 * @Title: RedisKeyConstant
 * @Author Macro Chen
 * @Package com.macro.tiny.controller
 * @Date 2022/12/18 17:06
 * @description: redis key 常量类
 */
public interface RedisKeyConstant {

    /**
     * 人员token key
     */
    public static final String USER_TOKEN_INFO = "user:token:%s:%s";
}
