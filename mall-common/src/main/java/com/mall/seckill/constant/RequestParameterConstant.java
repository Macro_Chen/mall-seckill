package com.mall.seckill.constant;

/**
 * @Title: RequestParameterConstant
 * @Author Macro Chen
 * @Package com.mall.seckill.constant
 * @Date 2022/12/26 21:56
 * @description: 请求参数常量
 */
public interface RequestParameterConstant {

    String LOGIN_PHONE = "phone";

    String LOGIN_VERIFY_CODE = "verifyCode";

    String CLIENT_ID = "client_id";

    String ATTRIBUTE_CLIENT_ID = "clientId";

    String SCOPES = "scopes";

    String STATE = "state";

    String PRINCIPAL_NAME = "principalName";

    String REDIRECT_URI = "redirectUri";

    String REDIRECT_URI_PARAM = "redirect_uri";

    String SCOPE = "scope";

    String RESPONSE_TYPE = "response_type";

    String STATE_PARAM = "state";

    String AUTHORIZATION_REQUEST = "authorizationRequest";

    String CLIENT_NAME = "clientName";

}
