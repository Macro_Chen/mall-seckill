package com.mall.seckill.constant;

/**
 * 消息常量
 * Created by macro on 2020/6/19.
 */
public class MessageConstant {

    public static final String LOGIN_SUCCESS = "登录成功!";

    public static final String LOGIN_FAILED = "登录失败!";

    public static final String USERNAME_PASSWORD_ERROR = "用户名或密码错误!";

    public static final String CREDENTIALS_EXPIRED = "该账户的登录凭证已过期，请重新登录!";

    public static final String ACCOUNT_DISABLED = "该账户已被禁用，请联系管理员!";

    public static final String ACCOUNT_LOCKED = "该账号已被锁定，请联系管理员!";

    public static final String ACCOUNT_EXPIRED = "该账号已过期，请联系管理员!";

    public static final String PERMISSION_DENIED = "没有访问权限，请联系管理员!";

    public static final String SERVICE_UNAVAILABLE = "服务不可用";
    public static final String EMPTY_PHONE_NUMBER = "手机号不能为空";
    public static final String EMPTY_VERIFY_CODE = "验证码不能为空";
    public static final String ERROR_VERIFY_CODE = "验证码错误";

    public static final String GET_TOKEN_FAILED = "获取token失败";
}
