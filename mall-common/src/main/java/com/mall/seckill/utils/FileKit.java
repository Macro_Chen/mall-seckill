package com.mall.seckill.utils;


import com.mall.seckill.enums.ImagesEnum;
import com.mall.seckill.exception.ApiException;

/**
 * @Title: FileKit
 * @Author Macro Chen
 * @Package com.macro.mall.util
 * @Date 2022/12/21 16:34
 * @description: 文件工具
 */
public class FileKit {


    /**
     * 获取文件的后缀名
     *
     * @param appendDot 是否拼接.
     * @return
     */
    public static String getFileSuffix(String fullFileName, boolean appendDot) {
        if (fullFileName == null || fullFileName.indexOf(".") < 0 || fullFileName.length() <= 1) {
            return "";
        }
        return (appendDot ? "." : "") + fullFileName.substring(fullFileName.lastIndexOf(".") + 1);
    }


    /**
     * 获取有效的图片格式， 返回null： 不支持的图片类型
     **/
    public static String getImgSuffix(String filePath) {

        String suffix = getFileSuffix(filePath, false).toLowerCase();
        if (ImagesEnum.ALLOW_UPLOAD_IMG_SUFFIX.contains(suffix)) {
            return suffix;
        }
        throw new ApiException("不支持的图片类型");
    }
}
