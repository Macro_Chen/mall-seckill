package com.mall.seckill.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * @Title: RequestUtils
 * @Author Macro Chen
 * @Package com.mall.seckill.utils
 * @Date 2022/12/24 0:51
 * @description: 请求工具类
 */
public class RequestUtils {

    public static String getParameter(HttpServletRequest request, String parameter) {
        return request.getParameter(parameter);
    }
}
