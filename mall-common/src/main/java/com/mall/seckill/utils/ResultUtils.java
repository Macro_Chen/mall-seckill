package com.mall.seckill.utils;

import com.mall.seckill.api.CommonResult;
import com.mall.seckill.api.ResultCode;

/**
 *请求结果工具类
 *
 * @author macroChen
 */
public class ResultUtils {

    public static <T> T getData(CommonResult<T> result, boolean isThrows) {
        if (result.getCode() == ResultCode.SUCCESS.getCode())
        {
            return result.getData();
        }
        AssertUtil.isTrue(isThrows, result.getMessage());
        return null;
    }
}
