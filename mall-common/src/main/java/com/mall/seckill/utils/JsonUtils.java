package com.mall.seckill.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author macroChen
 */
public class JsonUtils {

    // 定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private final static Logger logger = LoggerFactory.getLogger(JsonUtils.class);

    /**
     * 将对象转换成json字符串。
     */
    public static String objectToJson(Object data) {
        try {
            return MAPPER.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            logger.info("json解析异常：{}", e.getMessage());
            //e.printStackTrace();
        }
        return null;
    }

    /**
     * 将json结果集转化为对象
     *
     * @param jsonData json数据
     * @param beanType 对象中的object类型
     * @param ignore   大小写脱敏 默认为false  需要改为true
     */
    public static <T> T jsonToPojo(String jsonData, Class<T> beanType, boolean ignore) {
        try {

            MAPPER.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, ignore);//大小写脱敏 默认为false  需要改为tru
            T t = MAPPER.readValue(jsonData, beanType);
            return t;
        } catch (Exception e) {
            logger.info("json解析异常：{}", e.getMessage());
        }
        return null;
    }

    /**
     * 将json结果集转化为对象 大小写不敏感设置
     */
    public static <T> T jsonToPojo(String jsonData, Class<T> beanType) {
        return jsonToPojo(jsonData, beanType, false);
    }


    /**
     * 将json数据转换成pojo对象list
     */
    public static <T> List<T> jsonToList(String jsonData, Class<T> beanType) {
        MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JavaType javaType = MAPPER.getTypeFactory().constructParametricType(List.class, beanType);
        try {
            return MAPPER.readValue(jsonData, javaType);
        } catch (Exception e) {
            logger.info("json解析异常：{}", e.getMessage());
        }
        return Collections.emptyList();
    }

    public static <T> List<T> jsonToMapList(String jsonData, Class<Map> beanType) {
        MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JavaType javaType = MAPPER.getTypeFactory().constructParametricType(List.class, beanType);
        try {
            return MAPPER.readValue(jsonData, javaType);
        } catch (Exception e) {
            logger.info("json解析异常：{}", e.getMessage());
        }

        return null;
    }


    public static String jsonToString(String jsonData, String property) {
        try {
            JsonNode jsonNode = MAPPER.readTree(jsonData);
            return jsonNode.path(property).asText();
        } catch (IOException e) {
            logger.info("json解析异常：{}", e.getMessage());
        }
        return null;
    }


    public static List<String> fileJsonToFileList(String json, String filePrefix) {
        List<String> files = JsonUtils.jsonToList(json, String.class);
        if (!CollectionUtils.isEmpty(files)) {
            List<String> collect = files.stream().map(val -> filePrefix + val).collect(Collectors.toList());
            files.clear();
            return collect;
        }
        return Collections.emptyList();
    }


    public static int getTreeSize(String jsonData) {

        if (StringUtils.isBlank(jsonData)) {
            return 0;
        }
        try {

            return MAPPER.readTree(jsonData).size();
        } catch (IOException e) {
            logger.info("json解析异常：{}", e.getMessage());
            return 0;
        }
    }

    public static List<JsonNode> findValues(String jsonData, String fieldName) {
        try {
            JsonNode jsonNode = MAPPER.readTree(jsonData);

            return jsonNode.findValues(fieldName);
        } catch (IOException e) {
            logger.info("json解析异常：{}", e.getMessage());
            return Collections.emptyList();
        }

    }


}
