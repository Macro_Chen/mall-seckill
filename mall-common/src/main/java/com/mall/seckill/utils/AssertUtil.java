package com.mall.seckill.utils;

import cn.hutool.core.util.StrUtil;
import com.mall.seckill.exception.ApiException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.function.Consumer;

/**
 * @author gcq
 * @create 2022-10-11
 */
public class AssertUtil<T> {

    private final T value;

    public AssertUtil(T value) {
        this.value = value;
    }

    /**
     * 判断字符串非空
     *
     * @param str
     * @param message
     */
    public static void isNotEmpty(String str, String... message) {
        if (StrUtil.isBlank(str)) {
            execute(message);
        }
    }

    public static void isNotEmpty(String str, Class<? extends RuntimeException> exceptionClazz, String... message) throws InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        if (StrUtil.isBlank(str)) {
            execute(exceptionClazz, message);
        }
    }

    /**
     * 判断对象非空
     *
     * @param obj
     * @param message
     */
    public static void isNotNull(Object obj, String... message) {
        if (obj == null) {
            execute(message);
        }
    }

    /**
     * 判断对象非空
     *
     * @param obj
     * @param message
     */
    public static void isNotNull(Object obj, Class<? extends RuntimeException> exceptionClazz, String... message) throws InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        if (obj == null) {
            execute(exceptionClazz, message);
        }
    }

    /**
     * 判断结果是否为真
     *
     * @param isTrue
     * @param message
     */
    public static void isTrue(boolean isTrue, String... message) {
        if (isTrue) {
            execute(message);
        }
    }

    /**
     * 判断结果是否为真
     *
     * @param isTrue
     * @param message
     * @param exceptionClazz
     */
    public static void isTrue(boolean isTrue, Class<? extends RuntimeException> exceptionClazz, String... message) throws InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        if (isTrue) {
            execute(exceptionClazz, message);
        }
    }

    public static <T> AssertUtil<T> value(T value) {
        return new AssertUtil<>(value);
    }

    public static <T> AssertUtil<T> value(Class<T> clazz) throws InstantiationException, IllegalAccessException {
        T t = clazz.newInstance();
        return new AssertUtil<>(t);
    }

    /**
     * 条件判断执行语句
     * @param condition 条件
     * @param consumer 条件为真执行的语句
     */
    public void condition(boolean condition, Consumer<? super T> consumer) {
        if (condition) {
            consumer.accept(value);
        }
    }

    /**
     * 最终执行方法
     *
     * @param message
     */
    private static void execute(String... message) {
        String msg = "Oops! Something was wrong.";
        if (message != null && message.length > 0) {
            msg = message[0];
        }
        throw new ApiException(msg);
    }


    /**
     * 最终执行方法
     *
     * @param message
     */
    private static void execute(Class<? extends RuntimeException> exceptionClazz, String... message) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        String msg = "Oops! Something was wrong.";
        if (message != null && message.length > 0) {
            msg = message[0];
        }
        // 获取有参构造
        Constructor<? extends RuntimeException> paramConstructor = exceptionClazz.getConstructor(String.class);
        throw paramConstructor.newInstance(msg);
    }
}