package com.mall.seckill.enums;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * 授权范围常量
 * @author macroChen
 */
public class ScopeWithDescriptionEnum {

    private static final String DEFAULT_DESCRIPTION = "我们无法提供有关此权限的信息";
    private static final Map<String, String> scopeDescriptions = new HashMap<>();

    static {
        scopeDescriptions.put(
                "user_info",
                "验证您的身份"
        );
        scopeDescriptions.put(
                "message.read",
                "了解您可以访问哪些权限"
        );
        scopeDescriptions.put(
                "message.write",
                "代表您行事"
        );
    }

    public final String scope;
    public final String description;

    ScopeWithDescriptionEnum(String scope) {
        this.scope = scope;
        this.description = scopeDescriptions.getOrDefault(scope, DEFAULT_DESCRIPTION) + " ";
    }

    public static Set<ScopeWithDescriptionEnum> withDescription(Set<String> scopes) {
        Set<ScopeWithDescriptionEnum> scopeWithDescriptions = new LinkedHashSet<>();
        for (String scope : scopes) {
            scopeWithDescriptions.add(new ScopeWithDescriptionEnum(scope));

        }
        return scopeWithDescriptions;
    }
}
