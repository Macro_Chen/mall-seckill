package com.mall.seckill.enums;

import java.util.HashSet;
import java.util.Set;

/**
 * @Title: ImagesEnum
 * @Author Macro Chen
 * @Package com.macro.mall.constant
 * @Date 2022/12/21 16:32
 * @description: 图片常量
 */
public class ImagesEnum {

    /**
     * 允许上传的的图片文件格式，需要与 WebSecurityConfig对应
     */
    public static final Set<String> ALLOW_UPLOAD_IMG_SUFFIX = new HashSet<>();
    static{
        ALLOW_UPLOAD_IMG_SUFFIX.add("jpg");
        ALLOW_UPLOAD_IMG_SUFFIX.add("png");
        ALLOW_UPLOAD_IMG_SUFFIX.add("jpeg");
        ALLOW_UPLOAD_IMG_SUFFIX.add("gif");
        ALLOW_UPLOAD_IMG_SUFFIX.add("mp4");
    }
}
