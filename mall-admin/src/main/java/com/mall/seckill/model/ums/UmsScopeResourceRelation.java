package com.mall.seckill.model.ums;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author macroChen
 * @since 2022-12-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ums_scope_resource_relation")
@ApiModel(value="UmsScopeResourceRelation对象", description="")
public class UmsScopeResourceRelation implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "作用域id")
    private Long scopeId;

    @ApiModelProperty(value = "资源id")
    private Long resourceId;


}
