package com.mall.seckill.model.ums;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author macroChen
 * @since 2022-12-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ums_oauth_scope")
@ApiModel(value="UmsOauthScope对象", description="")
public class UmsOauthScope implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    @ApiModelProperty(value = "授权作用域")
    private String scope;

    @ApiModelProperty(value = "作用域描述")
    private String description;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;


}
