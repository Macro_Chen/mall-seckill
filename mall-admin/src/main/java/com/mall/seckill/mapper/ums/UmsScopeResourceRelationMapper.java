package com.mall.seckill.mapper.ums;

import com.mall.seckill.model.ums.UmsScopeResourceRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author macroChen
 * @since 2022-12-29
 */
public interface UmsScopeResourceRelationMapper extends BaseMapper<UmsScopeResourceRelation> {

}
