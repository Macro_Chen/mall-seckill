package com.mall.seckill.mapper.ums;

import com.mall.seckill.model.ums.UmsOauthScope;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author macroChen
 * @since 2022-12-29
 */
public interface UmsOauthScopeMapper extends BaseMapper<UmsOauthScope> {

}
