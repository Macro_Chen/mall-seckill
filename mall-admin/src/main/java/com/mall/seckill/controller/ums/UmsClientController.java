package com.mall.seckill.controller.ums;

import com.mall.seckill.api.CommonResult;
import com.mall.seckill.domain.ClientDto;
import com.mall.seckill.service.impl.ums.UmsClientServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈述智  梦想开始远航的地方
 * @date 2021/12/15 17:56
 * description 客户端管理
 */
@Api(tags = "UmsClientController",value = "客户端管理")
@RestController
@RequestMapping("/client")
public class UmsClientController {
    @Autowired
    private UmsClientServiceImpl umsClientService;

    @ApiOperation(value = "查询客户端信息")
    @RequestMapping(value = "/loadClientByClientId", method = RequestMethod.GET)
    public CommonResult<ClientDto> loadClientByClientId(@RequestParam String clientId){
        return CommonResult.success(umsClientService.loadClientByClientName(clientId));
    }
}
