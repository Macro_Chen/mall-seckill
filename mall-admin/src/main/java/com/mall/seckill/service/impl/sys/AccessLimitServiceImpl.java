package com.mall.seckill.service.impl.sys;

import com.google.common.util.concurrent.RateLimiter;
import com.mall.seckill.service.sys.AccessLimitService;
import org.springframework.stereotype.Service;

/**
 * 限流.
 * 使用了Google guava的RateLimiter
 */
@Service
public class AccessLimitServiceImpl implements AccessLimitService {
    /**
     * 每秒钟只发出50个令牌，拿到令牌的请求才可以进入正常业务逻辑
     */
    private RateLimiter seckillRateLimiter = RateLimiter.create(50);

    /**
     * 尝试获取令牌
     * @return boolean
     */
    @Override
    public boolean tryAcquire() {
        return seckillRateLimiter.tryAcquire();
    }
}
