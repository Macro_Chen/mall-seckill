package com.mall.seckill.service.impl.ums;

import com.mall.seckill.model.ums.UmsScopeResourceRelation;
import com.mall.seckill.mapper.ums.UmsScopeResourceRelationMapper;
import com.mall.seckill.service.ums.UmsScopeResourceRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author macroChen
 * @since 2022-12-29
 */
@Service
public class UmsScopeResourceRelationServiceImpl extends ServiceImpl<UmsScopeResourceRelationMapper, UmsScopeResourceRelation> implements UmsScopeResourceRelationService {

}
