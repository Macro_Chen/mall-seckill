package com.mall.seckill.service.impl.ums;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.BCrypt;
import cn.hutool.json.JSONUtil;
import com.github.pagehelper.PageHelper;
import com.mall.seckill.api.CommonResult;
import com.mall.seckill.api.ResultCode;
import com.mall.seckill.constant.AuthConstant;
import com.mall.seckill.constant.CommonConstant;
import com.mall.seckill.dao.UmsAdminDao;
import com.mall.seckill.dao.UmsAdminRoleRelationDao;
import com.mall.seckill.domain.ClientDto;
import com.mall.seckill.domain.UserDto;
import com.mall.seckill.dto.ums.UmsAdminLoginParam;
import com.mall.seckill.dto.ums.UmsAdminParam;
import com.mall.seckill.dto.ums.UpdateAdminPasswordParam;
import com.mall.seckill.exception.Asserts;
import com.mall.seckill.mapper.ums.UmsAdminMapper;
import com.mall.seckill.model.ums.UmsAdmin;
import com.mall.seckill.model.ums.UmsAdminExample;
import com.mall.seckill.model.ums.UmsRole;
import com.mall.seckill.service.impl.sys.AccessLimitServiceImpl;
import com.mall.seckill.service.AuthService;
import com.mall.seckill.service.ums.UmsAdminCacheService;
import com.mall.seckill.service.ums.UmsAdminService;
import com.mall.seckill.service.ums.UmsClientService;
import com.mall.seckill.utils.AssertUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * UmsAdminService实现类
 * Created by macro on 2018/4/26.
 */
@Service
public class UmsAdminServiceImpl implements UmsAdminService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UmsAdminServiceImpl.class);
    @Autowired
    private UmsAdminMapper adminMapper;
    @Autowired
    private UmsAdminRoleRelationDao adminRoleRelationDao;
    @Autowired
    private AuthService authService;
    @Autowired
    private UmsAdminCacheService adminCacheService;
    @Autowired
    private UmsClientService umsClientService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private AccessLimitServiceImpl accessLimitService;
    @Autowired
    private UmsAdminDao umsAdminDao;
    @Autowired
    private UmsAdminCacheService umsAdminCacheService;

    @Override
    public UmsAdmin register(UmsAdminParam umsAdminParam) {
        UmsAdmin umsAdmin = new UmsAdmin();
        BeanUtils.copyProperties(umsAdminParam, umsAdmin);
        umsAdmin.setCreateTime(new Date());
        umsAdmin.setStatus(1);
        //查询是否有相同用户名的用户
        UmsAdminExample example = new UmsAdminExample();
        example.createCriteria().andUsernameEqualTo(umsAdmin.getUsername());
        List<UmsAdmin> umsAdminList = adminMapper.selectByExample(example);
        if (umsAdminList.size() > 0) {
            return null;
        }
        //将密码进行加密操作
        String encodePassword = BCrypt.hashpw(umsAdmin.getPassword());
        umsAdmin.setPassword(encodePassword);
        adminMapper.insert(umsAdmin);
        return umsAdmin;
    }

    @Override
    public UmsAdmin getAdminByUsername(String username) {
        // 条件
        boolean condition = false;
        // 从缓存当中获取用户信息
        UmsAdmin umsAdmin = umsAdminCacheService.getAdmin(username);
        if (umsAdmin != null)
        {
            return umsAdmin;
        }
        // 根据用户名称查询用户
        umsAdmin = umsAdminDao.selectOne(UmsAdmin.gw().eq(UmsAdmin::getUsername, username).or().eq(UmsAdmin::getPhone, username));
        LOGGER.info("用户信息:{}", umsAdmin);
        condition = Objects.nonNull(umsAdmin);
        // 设置缓存
        umsAdminCacheService.setAdmin(umsAdmin, condition
                ? () -> { return username;}
                : null, condition);
        // 返回用户信息
        return condition ? umsAdmin : null;
    }

    @Override
    public UmsAdmin getCurrentAdmin() {
        /*
         * AuthConstant.USER_TOKEN_HEADER 该请求头已经在网关的时候添加到请求头当中了
         */
        String userStr = request.getHeader(AuthConstant.USER_TOKEN_HEADER);
        if (StrUtil.isEmpty(userStr)) {
            LOGGER.error("该请求头当中没有token,或者请求头当中user是空的");
            Asserts.fail(ResultCode.UNAUTHORIZED);
        }
        LOGGER.info("用户信息Http请求头: {}", userStr);
        UserDto userDto = JSONUtil.toBean(userStr, UserDto.class);
        UmsAdmin umsAdmin = adminCacheService.getAdmin(userDto.getId());
        if (Objects.isNull(umsAdmin)) {
            umsAdmin = adminMapper.selectByPrimaryKey(userDto.getId());
        }
        return umsAdmin;
    }

    @Override
    public CommonResult<Object> login(UmsAdminLoginParam param) {
        // 限流
        AssertUtil.isTrue(!accessLimitService.tryAcquire(), CommonConstant.TOO_MANY_VISITORS);
        // 根据客户端名称查询客户端信息
        ClientDto clientInfo = umsClientService.loadClientByClientName(param.getClient());
        // 找不到客户端信息
        AssertUtil.isNotNull(clientInfo, CommonConstant.NOT_FOUND_CLIENT);
        // 不包含授权类型
        AssertUtil.isTrue(!clientInfo.getAuthorizedGrantTypes().contains(param.getGrantType()),
                CommonConstant.NOT_FOUND_GRANT_TYPE);
        Map<String, String> params = new HashMap<>(6);
        // 设置客户端信息以及登录信息
        params.put("client_id", param.getClient());
        params.put("client_secret", param.getClientSecret());
        params.put("grant_type", param.getGrantType());
        params.put("username", param.getUsername());
        params.put("password", param.getPassword());
        params.put("scopes", "all");
        LOGGER.info("授权参数:{}", params);
        return authService.getAccessToken(params, CommonConstant.FROM_IN);
    }

    @Override
    public List<UmsRole> getRoleList(Long adminId) {
        return adminRoleRelationDao.getRoleList(adminId);
    }


    @Override
    public UserDto loadUserByUsername(String username) {
        //获取用户信息
        UmsAdmin admin = getAdminByUsername(username);
        if (admin != null) {
            List<UmsRole> roleList = getRoleList(admin.getId());
            UserDto userDTO = new UserDto();
            BeanUtils.copyProperties(admin, userDTO);
            if (CollUtil.isNotEmpty(roleList)) {
                List<String> roleStrList = roleList.stream().map(item -> item.getId() + "_" + item.getName()).collect(Collectors.toList());
                userDTO.setRoles(roleStrList);
            }
            return userDTO;
        }
        return null;
    }

    @Override
    public List<UmsAdmin> list(String keyword, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        UmsAdminExample example = new UmsAdminExample();
        UmsAdminExample.Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(keyword)) {
            criteria.andUsernameLike("%" + keyword + "%");
            example.or(example.createCriteria().andNickNameLike("%" + keyword + "%"));
        }
        return adminMapper.selectByExample(example);
    }

    @Override
    public UmsAdmin getItem(Long id) {
        return adminMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updatePassword(UpdateAdminPasswordParam param) {
        if (StrUtil.isEmpty(param.getUsername())
                || StrUtil.isEmpty(param.getOldPassword())
                || StrUtil.isEmpty(param.getNewPassword())) {
            return -1;
        }
        UmsAdminExample example = new UmsAdminExample();
        example.createCriteria().andUsernameEqualTo(param.getUsername());
        List<UmsAdmin> adminList = adminMapper.selectByExample(example);
        if (CollUtil.isEmpty(adminList)) {
            return -2;
        }
        UmsAdmin umsAdmin = adminList.get(0);
        if (!BCrypt.checkpw(param.getOldPassword(), umsAdmin.getPassword())) {
            //密码不匹配
            return -3;
        }
        umsAdmin.setPassword(BCrypt.hashpw(param.getNewPassword()));
        adminMapper.updateByPrimaryKey(umsAdmin);
        adminCacheService.delAdmin(umsAdmin.getId());
        return 1;
    }

    @Override
    public int update(Long id, UmsAdmin admin) {
        admin.setId(id);
        UmsAdmin rawAdmin = adminMapper.selectByPrimaryKey(id);
        if (rawAdmin.getPassword().equals(admin.getPassword())) {
            //与原加密密码相同的不需要修改
            admin.setPassword(null);
        } else {
            //与原加密密码不同的需要加密修改
            if (StrUtil.isEmpty(admin.getPassword())) {
                admin.setPassword(null);
            } else {
                admin.setPassword(BCrypt.hashpw(admin.getPassword()));
            }
        }
        int count = adminMapper.updateByPrimaryKeySelective(admin);
        adminCacheService.delAdmin(id);
        return count;
    }

    @Override
    public int delete(Long id) {
        int count = adminMapper.deleteByPrimaryKey(id);
        adminCacheService.delAdmin(id);
        return count;
    }
}
