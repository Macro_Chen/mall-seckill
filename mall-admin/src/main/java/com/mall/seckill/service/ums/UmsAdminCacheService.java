package com.mall.seckill.service.ums;

import com.mall.seckill.model.ums.UmsAdmin;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * 后台用户缓存操作类
 * Created by macro on 2020/3/13.
 */
public interface UmsAdminCacheService {
    /**
     * 删除后台用户缓存
     */
    void delAdmin(Long adminId);

    /**
     * 获取缓存后台用户信息
     */
    UmsAdmin getAdmin(Long adminId);

    /**
     * 通过用户名获取缓存后台用户信息
     */
    UmsAdmin getAdmin(String username);

    /**
     * 通过用户名设置缓存后台用户信息
     * @param admin 用户信息
     * @param supplier key
     * @param condition 设置缓存的条件
     */
    void setAdmin(UmsAdmin admin, Supplier<Object> supplier, Boolean condition);

    /**
     * 设置缓存后台用户信息
     */
    void setAdmin(UmsAdmin admin, Long adminId);
}
