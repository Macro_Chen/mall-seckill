package com.mall.seckill.service.ums;

import com.mall.seckill.domain.ClientDto;
import com.mall.seckill.dto.ums.UmsClientAuthorizedGrantType;

import java.util.List;

/**
 * @author 陈述智  梦想开始远航的地方
 * @date 2021/12/15 17:38
 * description
 */
public interface UmsClientService {

    /**
     * 根据客户端id查询所有的授权类型
     * @param clientId 客户端id
     * @return 授权类型集合
     */
    List<UmsClientAuthorizedGrantType> getAuthorizedGrantTypes(Long clientId);

    /**
     * 获取客户端信息
     * @param clientName 客户端名称
     * @return 客户端信息
     */
    ClientDto loadClientByClientName(String clientName);
}
