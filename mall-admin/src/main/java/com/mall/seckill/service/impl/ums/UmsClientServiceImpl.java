package com.mall.seckill.service.impl.ums;

import cn.hutool.core.collection.CollUtil;
import com.mall.seckill.dao.UmsClientDao;
import com.mall.seckill.dao.UmsClientRelationDao;
import com.mall.seckill.domain.ClientDto;
import com.mall.seckill.dto.ums.UmsClient;
import com.mall.seckill.dto.ums.UmsClientAuthorizedGrantType;
import com.mall.seckill.service.ums.UmsClientService;
import com.mall.seckill.utils.AssertUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author 陈述智  梦想开始远航的地方
 * @date 2021/12/15 17:40
 * description
 */
@Service
public class UmsClientServiceImpl implements UmsClientService {

    private static final Logger log = LoggerFactory.getLogger(UmsClientServiceImpl.class);

    @Autowired
    private UmsClientDao umsClientDao;
    @Autowired
    private UmsClientRelationDao umsClientRelationDao;

    /**
     * 根据客户端id获取授权类型
     * @param clientId 客户端id
     * @return 授权类型集合
     */
    @Override
    public List<UmsClientAuthorizedGrantType> getAuthorizedGrantTypes(Long clientId)
    {
        return umsClientRelationDao.getAuthorizedGrantTypes(clientId);
    }

    /**
     * 加载的客户端类型及名称
     * @param clientId 客户端id
     * @return ClientDto
     */
    @Override
    public ClientDto loadClientByClientName(String clientId)
    {
        UmsClient umsClient = umsClientDao.selectClientByClientId(clientId);
        log.info("查询客户端信息：{}", umsClient);

        return Optional.ofNullable(umsClient).map(item -> {
            ClientDto clientDto = new ClientDto();
            BeanUtils.copyProperties(item, clientDto);
            // 查询该客户端的所有授权类型
            List<UmsClientAuthorizedGrantType> grantTypes = getAuthorizedGrantTypes(umsClient.getId());
            // 设置客户端授权模式列表
            boolean condition = CollUtil.isNotEmpty(grantTypes);
            AssertUtil.value(grantTypes).condition(condition, (target) -> {
                List<String> collect = grantTypes
                        .stream()
                        .map(UmsClientAuthorizedGrantType::getAuthorizedGrantType)
                        .collect(Collectors.toList());
                clientDto.setAuthorizedGrantTypes(collect);
            });

            return clientDto;
        }).orElse(null);
    }

}
