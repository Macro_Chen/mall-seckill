package com.mall.seckill.service.impl.ums;

import com.mall.seckill.model.ums.UmsOauthScope;
import com.mall.seckill.mapper.ums.UmsOauthScopeMapper;
import com.mall.seckill.service.ums.UmsOauthScopeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author macroChen
 * @since 2022-12-29
 */
@Service
public class UmsOauthScopeServiceImpl extends ServiceImpl<UmsOauthScopeMapper, UmsOauthScope> implements UmsOauthScopeService {

}
