package com.mall.seckill.service.impl.ums;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mall.seckill.constant.AuthConstant;
import com.mall.seckill.mapper.ums.*;
import com.mall.seckill.model.ums.*;
import com.mall.seckill.service.RedisService;
import com.mall.seckill.service.ums.UmsResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

/**
 * author: macro陈
 * Created date：2021/12/19.
 */
@Service
@Slf4j
public class UmsRerourceServiceImpl implements UmsResourceService {
    @Autowired
    private UmsResourceMapper resourceMapper;
    @Autowired
    private UmsRoleMapper roleMapper;
    @Autowired
    private UmsRoleResourceRelationMapper roleResourceRelationMapper;
    @Autowired
    private RedisService redisService;
    @Autowired
    private UmsOauthScopeMapper umsOauthScopeMapper;
    @Autowired
    private UmsScopeResourceRelationMapper umsScopeResourceRelationMapper;
    @Value("${spring.application.name}")
    private String applicationName;

    @Override
    @PostConstruct
    public Map<String,List<String>> initResourceRolesMap() {
        Map<String,List<String>> resourceRoleMap = new TreeMap<>();
        Map<String,List<String>> resourceOfScopeMap = new TreeMap<>();
        // 所有资源
        List<UmsResource> resourceList = resourceMapper.selectByExample(new UmsResourceExample());
        // 所有作用域
        List<UmsOauthScope> scopeList = umsOauthScopeMapper.selectList(new QueryWrapper<>());
        // 所有作用域与资源关系
        List<UmsScopeResourceRelation> umsScopeResourceRelations = umsScopeResourceRelationMapper.selectList(new QueryWrapper<>());
        // 所有角色
        List<UmsRole> roleList = roleMapper.selectByExample(new UmsRoleExample());
        // 所有角色与资源关系
        List<UmsRoleResourceRelation> relationList = roleResourceRelationMapper.selectByExample(new UmsRoleResourceRelationExample());
        // 改变根据角色校验的资源格式
        for (UmsResource resource : resourceList) {
            Set<Long> roleIds = relationList.stream().filter(item -> item.getResourceId().equals(resource.getId())).map(UmsRoleResourceRelation::getRoleId).collect(Collectors.toSet());
            List<String> roleNames = roleList.stream().filter(item -> roleIds.contains(item.getId())).map(item -> item.getId() + "_" + item.getName()).collect(Collectors.toList());
            List<Long> scopeIds = umsScopeResourceRelations.stream().filter(item -> item.getResourceId().equals(resource.getId())).map(UmsScopeResourceRelation::getScopeId).collect(Collectors.toList());
            List<String> scopeName = scopeList.stream().filter(item -> scopeIds.contains(item.getId())).map(UmsOauthScope::getScope).collect(Collectors.toList());
            if (CollUtil.isNotEmpty(roleNames)) {
                resourceRoleMap.put("/"+applicationName+resource.getUrl(),roleNames);
            }
            if (CollUtil.isNotEmpty(scopeName)) {
                resourceOfScopeMap.put("/"+applicationName+resource.getUrl(), scopeName);
            }
        }
        redisService.del(AuthConstant.RESOURCE_ROLES_MAP_KEY);
        redisService.del(AuthConstant.RESOURCE_SCOPES_MAP_KEY);
        log.info("加载系统接口-角色-资源至缓存:{}", resourceRoleMap.size());
        log.info("加载系统接口-作用域-资源至缓存:{}", resourceOfScopeMap.size());
        redisService.hSetAll(AuthConstant.RESOURCE_ROLES_MAP_KEY, resourceRoleMap);
        redisService.hSetAll(AuthConstant.RESOURCE_SCOPES_MAP_KEY, resourceOfScopeMap);
        return resourceRoleMap;
    }
}
