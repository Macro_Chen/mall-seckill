package com.mall.seckill.service.ums;

import com.mall.seckill.model.ums.UmsScopeResourceRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author macroChen
 * @since 2022-12-29
 */
public interface UmsScopeResourceRelationService extends IService<UmsScopeResourceRelation> {

}
