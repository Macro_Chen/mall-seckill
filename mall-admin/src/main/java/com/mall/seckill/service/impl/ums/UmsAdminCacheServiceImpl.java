package com.mall.seckill.service.impl.ums;

import com.mall.seckill.model.ums.UmsAdmin;
import com.mall.seckill.service.RedisService;
import com.mall.seckill.service.ums.UmsAdminCacheService;
import com.mall.seckill.service.ums.UmsAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * UmsAdminCacheService实现类
 * Created by macro on 2020/3/13.
 */
@Service
public class UmsAdminCacheServiceImpl implements UmsAdminCacheService {
    @Autowired
    private RedisService redisService;
    @Value("${redis.database}")
    private String REDIS_DATABASE;
    @Value("${redis.expire.common}")
    private Long REDIS_EXPIRE;
    @Value("${redis.key.admin}")
    private String REDIS_KEY_ADMIN;

    @Override
    public void delAdmin(Long adminId) {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_ADMIN + ":" + adminId;
        redisService.del(key);
    }

    @Override
    public UmsAdmin getAdmin(Long adminId) {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_ADMIN + ":" + adminId;
        return (UmsAdmin) redisService.get(key);
    }

    @Override
    public UmsAdmin getAdmin(@NotNull String username) {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_ADMIN + ":" + username;
        return (UmsAdmin) redisService.get(key);
    }

    @Override
    public void setAdmin(@NotNull UmsAdmin admin, @NotNull Supplier<Object> supplier, Boolean condition) {
        // 满足条件则添加缓存
        if (condition)
        {
            String key = REDIS_DATABASE + ":" + REDIS_KEY_ADMIN + ":" + supplier.get();
            redisService.set(key, admin, REDIS_EXPIRE);
        }
    }

    @Override
    public void setAdmin(@NotNull UmsAdmin admin, @NotNull Long adminId) {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_ADMIN + ":" + adminId;
        redisService.set(key, admin, REDIS_EXPIRE);
    }
}
