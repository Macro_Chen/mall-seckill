package com.mall.seckill.dto.ums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 陈述智  梦想开始远航的地方
 * @date 2021/12/15 17:19
 * @Description 授权类型BO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UmsClientAuthorizedGrantType implements Serializable {
    /**
     * 授权类型id
     */
    private Long id;

    /**
     * 授权类型
     */
    private String authorizedGrantType;
}
