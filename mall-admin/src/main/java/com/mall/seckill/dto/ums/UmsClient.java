package com.mall.seckill.dto.ums;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 陈述智  梦想开始远航的地方
 * @date 2021/12/15 17:17
 * description
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ums_client")
@ApiModel(value="UmsClient对象", description="")
public class UmsClient implements Serializable {

    public static LambdaQueryWrapper<UmsClient> gw(){
        return new LambdaQueryWrapper<UmsClient>();
    }

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "客户端id")
    private String clientId;

    @ApiModelProperty(value = "客户端名称")
    private String clientName;

    @ApiModelProperty(value = "客户端密钥")
    private String clientSecret;

    @ApiModelProperty(value = "客户端授权码模式")
    private String redirectUri;

    @ApiModelProperty(value = "accessToken过期时间")
    private Integer accessTokenValidity;

    @ApiModelProperty(value = "刷新token过期时间")
    private Integer refreshTokenValidity;

    @ApiModelProperty(value = "是否自动授权")
    private String autoApprove;

    @ApiModelProperty(value = "权限区域")
    private String scope;

    @ApiModelProperty(value = "附加说明")
    private String additionalInformation;


}
