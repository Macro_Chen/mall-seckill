package com.mall.seckill.dto.ums;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;

/**
 * 用户登录参数
 *
 * @author macro
 * @date 2018/4/26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UmsAdminLoginParam {

    @NotEmpty(message = "用户名不能为空")
    @ApiModelProperty(value = "用户名", required = true)
    private String username;

    @NotEmpty(message = "密码不能为空")
    @ApiModelProperty(value = "密码", required = true)
    private String password;

    @NotEmpty(message = "客户端不能为空")
    @ApiModelProperty(value = "客户端", required = true)
    private String client;

    @NotEmpty(message = "客户端密码不能为空")
    @ApiModelProperty(value = "客户端密码", required = true)
    private String clientSecret;

    @NotEmpty(message = "授权类型不允许为空")
    @ApiModelProperty(value = "授权类型不允许为空", required = true)
    private String grantType;
}
