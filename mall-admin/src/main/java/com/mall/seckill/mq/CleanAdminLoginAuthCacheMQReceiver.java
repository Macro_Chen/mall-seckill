/*
 * Copyright (c) 2021-2031, 河北计全科技有限公司 (https://www.jeequan.com & jeequan@126.com).
 * <p>
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE 3.0;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mall.seckill.mq;

import com.mall.seckill.model.CleanAdminLoginAuthCacheMQ;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 接收MQ消息
 * 业务： 清除登录信息
 *
 * @Title: RabbitMQSender
 * @Author Macro Chen
 * @Package com.macro.tiny.vender.rabbitMQ.receiver
 * @Date 2022/12/21 14:10
 * @description: 清除登录信息
 */
@Slf4j
@Component
public class CleanAdminLoginAuthCacheMQReceiver implements CleanAdminLoginAuthCacheMQ.IMQReceiver {

    @Override
    public void receive(CleanAdminLoginAuthCacheMQ.MsgPayload payload) {

        log.info("成功接收删除商户用户登录的订阅通知, msg={}", payload);
        // 字符串转List<Long>
        List<Long> userIdList = payload.getUserIdList();
    }
}
