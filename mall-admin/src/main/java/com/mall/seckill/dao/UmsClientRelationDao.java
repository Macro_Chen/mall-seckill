package com.mall.seckill.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mall.seckill.dto.ums.UmsClientAuthorizedGrantType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 陈述智  梦想开始远航的地方
 * @date 2021/12/15 17:31
 * description
 */
@Mapper
public interface UmsClientRelationDao {

    List<UmsClientAuthorizedGrantType> getAuthorizedGrantTypes(Long clientId);
}
