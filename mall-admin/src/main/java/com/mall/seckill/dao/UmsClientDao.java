package com.mall.seckill.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mall.seckill.dto.ums.UmsClient;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈述智  梦想开始远航的地方
 * @date 2021/12/15 17:23
 * description
 */
@Mapper
public interface UmsClientDao extends BaseMapper<UmsClient> {
    /**
     * 根据客户端名称获取客户端信息
     * @param clientId 客户端id
     * @return  客户端信息
     */
    UmsClient selectClientByClientId(String clientId);

}
