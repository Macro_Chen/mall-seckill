package com.mall.seckill.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mall.seckill.model.ums.UmsAdmin;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Title: UmsAdminDao
 * @Author Macro Chen
 * @Package com.mall.seckill.dao
 * @Date 2022/12/23 13:36
 * @description: 用户dao
 */
@Mapper
public interface UmsAdminDao extends BaseMapper<UmsAdmin> {

    UmsAdmin selectUserByUsernameAndClientName(String username, String clientName);
}
