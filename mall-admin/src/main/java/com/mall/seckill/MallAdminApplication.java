package com.mall.seckill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class MallAdminApplication {

    public static void main(String[] args) {  //main方法 一个程序入口   启动类
        SpringApplication.run(MallAdminApplication.class, args);
        System.out.println("mall-admin启动成功");
    }
}
