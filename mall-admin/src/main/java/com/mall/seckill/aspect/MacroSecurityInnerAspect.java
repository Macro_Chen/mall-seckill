package com.mall.seckill.aspect;

import cn.hutool.core.util.StrUtil;
import com.mall.seckill.annotation.Inner;
import com.mall.seckill.api.ResultCode;
import com.mall.seckill.constant.CommonConstant;
import com.mall.seckill.exception.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @Title: MacroSecurityInnerAspect
 * @Author Macro Chen
 * @Package com.mall.seckill.aspect
 * @Date 2022/12/31 15:23
 * @description: 内部服务调用标识切面
 */
@Aspect
@Component
@Order(1)
@ConditionalOnProperty(prefix = "inner", name = "enable")
@Slf4j
public class MacroSecurityInnerAspect {
    @Autowired
    private  HttpServletRequest request;

    @Pointcut("execution(public * com.mall.seckill.controller.*.*.*(..)))||execution(public * com.mall.seckill.controller.*.*(..))")
    public void securityInnerAspect() {
    }

    @Before("securityInnerAspect() && @annotation(inner)")
    public void doBefore(JoinPoint joinPoint, Inner inner) throws Throwable {
    }

    @AfterReturning(value = "securityInnerAspect() && @annotation(inner)", returning = "ret")
    public void doAfterReturning(Object ret, Inner inner) throws Throwable {
    }

    @Around("securityInnerAspect() && @annotation(inner)")
    public Object around(ProceedingJoinPoint point, Inner inner) throws Throwable {
        String header = request.getHeader(CommonConstant.FROM);
        log.info("是否内部服务请求{}", header);
        if (inner.value() && !StrUtil.equals(CommonConstant.FROM_IN, header)) {
            throw new ApiException(ResultCode.DENY_ACCESS.getMessage());
        }
        return point.proceed();
    }
}
