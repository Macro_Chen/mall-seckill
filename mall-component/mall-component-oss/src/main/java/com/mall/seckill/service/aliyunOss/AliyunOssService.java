package com.mall.seckill.service.aliyunOss;

import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONUtil;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.model.MatchMode;
import com.aliyun.oss.model.PolicyConditions;
import com.mall.seckill.config.AliyunOssConfig;
import com.mall.seckill.config.OssFileConfig;
import com.mall.seckill.constant.Constants;
import com.mall.seckill.constant.NumberConstant;
import com.mall.seckill.domain.OssCallbackParam;
import com.mall.seckill.domain.OssCallbackResult;
import com.mall.seckill.domain.OssPolicyResult;
import com.mall.seckill.service.IOssService;
import com.mall.seckill.utils.AssertUtil;
import com.mall.seckill.utils.FileKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Title: AliyunOssService
 * @Author Macro Chen
 * @Package com.macro.mall.service.aliyunOss
 * @Date 2022/12/21 16:10
 * @description: 阿里云oss上传
 */
@Service
public class AliyunOssService implements IOssService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AliyunOssConfig aliyunOssConfig;
    @Autowired
    private OSSClient ossClient;

    /**
     * 上传文件 & 生成下载/预览URL
     * 由于带宽很贵 上传尽量在前端上传 不建议后端上传
     **/
    @Override
    public Map<String, Object> upload2PreviewUrl(MultipartFile file) throws IOException {
        // 1.判断文件是否支持
        String fileSuffix = FileKit.getFileSuffix(file.getOriginalFilename(), false);
        AssertUtil.isTrue(!OssFileConfig.IMG_SUFFIX.contains(fileSuffix), "不支持上传该格式文件");
        // 2.判断是否超出指定大小
        AssertUtil.isTrue(!OssFileConfig.isMaxSizeLimit(file.getSize()), "上传大小请限制在[" + OssFileConfig.maxSize / 1024 / 1024 + "M]以内！");
        // 3.上传文件
        return uploadFile(file.getInputStream(), file.getOriginalFilename());
    }

    /**
     * 签名生成
     */
    @Override
    public OssPolicyResult policy() {
        OssPolicyResult result = new OssPolicyResult();
        // 存储目录
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String dir = aliyunOssConfig.ALIYUN_OSS_DIR_PREFIX + sdf.format(new Date());
        // 签名有效期
        long expireEndTime = System.currentTimeMillis() + aliyunOssConfig.ALIYUN_OSS_EXPIRE * 1000L;
        Date expiration = new Date(expireEndTime);
        // 文件大小
        long maxSize = (long) aliyunOssConfig.ALIYUN_OSS_MAX_SIZE * 1024 * 1024;
        // 回调
        OssCallbackParam callback = new OssCallbackParam();
        callback.setCallbackUrl(aliyunOssConfig.ALIYUN_OSS_CALLBACK);
        callback.setCallbackBody("filename=${object}&size=${size}&mimeType=${mimeType}&height=${imageInfo.height}&width=${imageInfo.width}");
        callback.setCallbackBodyType("application/x-www-form-urlencoded");
        // 提交节点
        String action = Constants.HTTPS + aliyunOssConfig.ALIYUN_OSS_BUCKET_NAME + "." + aliyunOssConfig.ALIYUN_OSS_ENDPOINT;
        try {
            PolicyConditions policyConds = new PolicyConditions();
            policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, maxSize);
            policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, dir);
            String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
            byte[] binaryData = postPolicy.getBytes(StandardCharsets.UTF_8);
            String policy = BinaryUtil.toBase64String(binaryData);
            String signature = ossClient.calculatePostSignature(postPolicy);
            String callbackData = BinaryUtil.toBase64String(JSONUtil.parse(callback).toString().getBytes("utf-8"));
            // 返回结果
            result.setAccessKeyId(ossClient.getCredentialsProvider().getCredentials().getAccessKeyId());
            result.setPolicy(policy);
            result.setSignature(signature);
            result.setDir(dir);
            result.setCallback(callbackData);
            result.setHost(action);
        } catch (Exception e) {
            LOGGER.error("签名生成失败", e);
        }
        return result;
    }

    /**
     * 阿里云oss上传回调
     *
     * @param request
     * @return
     */
    @Override
    public OssCallbackResult callback(HttpServletRequest request) {
        OssCallbackResult result = new OssCallbackResult();
        String filename = request.getParameter("filename");
        filename = Constants.HTTPS.concat(aliyunOssConfig.ALIYUN_OSS_BUCKET_NAME).concat(".").concat(aliyunOssConfig.ALIYUN_OSS_ENDPOINT).concat("/").concat(filename);
        result.setFilename(filename);
        result.setSize(request.getParameter("size"));
        result.setMimeType(request.getParameter("mimeType"));
        result.setWidth(request.getParameter("width"));
        result.setHeight(request.getParameter("height"));
        return result;
    }

    /**
     * 单文件上传
     *
     * @param in       文件输入流
     * @param fileName 文件名称
     * @return Map
     */
    @Override
    public Map<String, Object> uploadFile(InputStream in, String fileName) {
        HashMap<String, Object> result = new HashMap<>(NumberConstant.TWO);
        // 存储目录
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        // TODO: 根据请求的模块进行分模块存储(可在OssFileConfig中定义模块常量放入HashSet中)
        fileName = aliyunOssConfig.ALIYUN_OSS_DIR_ORGANIZATIONEXCELPREFIX
                + IdUtil.fastSimpleUUID() + sdf.format(new Date()) + fileName;
        ossClient.putObject(aliyunOssConfig.ALIYUN_OSS_BUCKET_NAME, fileName, in);
        // 文件访问地址
        String url = Constants.HTTPS.concat(aliyunOssConfig.ALIYUN_OSS_BUCKET_NAME).concat(".")
                .concat(aliyunOssConfig.ALIYUN_OSS_ENDPOINT).concat("/").concat(fileName);
        result.put("url", url);
        result.put("fileName", fileName.substring(fileName.lastIndexOf("/") + 1));
        return result;
    }
}
