package com.mall.seckill.config;

import com.mall.seckill.constant.OssSavePlaceEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @Title: OssFileConfig
 * @Author Macro Chen
 * @Package com.macro.mall.config
 * @Date 2022/12/21 16:36
 * @description: 上传文件配置类
 */
@Data
@AllArgsConstructor
public class OssFileConfig {

    /**
     * 图片类型后缀格式
     **/
    public static final Set<String> IMG_SUFFIX = new HashSet<String>(Arrays.asList("jpg", "png", "jpeg", "gif"));

    /**
     * 全部后缀格式的文件标识符
     **/
    public static final String ALL_SUFFIX_FLAG = "*";

    /**
     * 不校验文件大小标识符
     **/
    public static final Long ALL_MAX_SIZE = -1L;

    /**
     * 单文件允许上传的最大文件大小的默认值(后端上传,前端上传时会有校验)
     **/
    public static final Long DEFAULT_MAX_SIZE = 5 * 1024 * 1024L;

    /**
     * 存储位置
     **/
    private OssSavePlaceEnum ossSavePlaceEnum;

    /**
     * 允许的文件后缀, 默认全部类型
     **/
    private Set<String> allowFileSuffix = new HashSet<>(Arrays.asList(ALL_SUFFIX_FLAG));

    /**
     * 允许的文件大小, 单位： Byte
     **/
    public static Long maxSize = DEFAULT_MAX_SIZE;


    /**
     * 是否在允许的文件类型后缀内
     **/
    public boolean isAllowFileSuffix(String fixSuffix) {

        //允许全部
        if (this.allowFileSuffix.contains(ALL_SUFFIX_FLAG)) {
            return true;
        }

        return this.allowFileSuffix.contains(StringUtils.defaultIfEmpty(fixSuffix, "").toLowerCase());
    }

    /**
     * 是否在允许的大小范围内
     **/
    public static boolean isMaxSizeLimit(Long fileSize) {

        //允许全部大小
        if (ALL_MAX_SIZE.equals(maxSize)) {
            return true;
        }

        return maxSize >= (fileSize == null ? 0L : fileSize);
    }

}
