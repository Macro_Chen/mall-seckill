package com.mall.seckill.controller;

import com.mall.seckill.api.CommonResult;
import com.mall.seckill.domain.OssCallbackResult;
import com.mall.seckill.domain.OssPolicyResult;
import com.mall.seckill.service.IOssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * @Title: OssController
 * @Author Macro Chen
 * @Package com.macro.mall.controller
 * @Date 2022/12/21 16:55
 * @description: oss controller
 */
@RestController
@Api(tags = "OssUploadController")
@RequestMapping("oss-com")
public class OssUploadController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private IOssService iOssService;

    @ApiOperation(value = "oss单文件上传")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public CommonResult<Map<String, Object>> upload(@RequestParam(name = "file") MultipartFile file) throws IOException {
        return CommonResult.success(iOssService.upload2PreviewUrl(file));
    }

    @ApiOperation(value = "Oss上传签名生成")
    @RequestMapping(value = "/policy", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<OssPolicyResult> policy() {
        OssPolicyResult result = iOssService.policy();
        log.info("生成oss签名:{}", result);
        return CommonResult.success(result);
    }

    @ApiOperation(value = "Oss上传成功回调")
    @RequestMapping(value = "callback", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<OssCallbackResult> callback(HttpServletRequest request) {
        OssCallbackResult ossCallbackResult = iOssService.callback(request);
        log.info("oss上传回调:{}", ossCallbackResult);
        return CommonResult.success(ossCallbackResult);
    }
}
