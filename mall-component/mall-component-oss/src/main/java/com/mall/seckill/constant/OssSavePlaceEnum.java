package com.mall.seckill.constant;

/**
 * @Title: OssSavePlaceEnum
 * @Author Macro Chen
 * @Package com.macro.mall.constant
 * @Date 2022/12/21 15:59
 * @description: oss 存储位置
 */
public enum OssSavePlaceEnum {

    /**
     * 公共读取
     */
    PUBLIC,

    /**
     * 私有存储
     */
    PRIVATE;

}
