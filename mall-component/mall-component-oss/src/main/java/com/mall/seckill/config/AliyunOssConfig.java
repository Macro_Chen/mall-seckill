package com.mall.seckill.config;

import com.aliyun.oss.OSSClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * OSS对象存储相关配置
 * Created by macro on 2018/5/17.
 */
@Configuration
@Component
public class AliyunOssConfig {
    @Value("${aliyun.oss.endpoint}")
    public String ALIYUN_OSS_ENDPOINT;
    @Value("${aliyun.oss.accessKeyId}")
    public String ALIYUN_OSS_ACCESSKEYID;
    @Value("${aliyun.oss.accessKeySecret}")
    public String ALIYUN_OSS_ACCESSKEYSECRET;
    @Value("${aliyun.oss.policy.expire}")
    public int ALIYUN_OSS_EXPIRE;
    @Value("${aliyun.oss.maxSize}")
    public int ALIYUN_OSS_MAX_SIZE;
    @Value("${aliyun.oss.callback}")
    public String ALIYUN_OSS_CALLBACK;
    @Value("${aliyun.oss.bucketName}")
    public String ALIYUN_OSS_BUCKET_NAME;
    @Value("${aliyun.oss.dir.prefix}")
    public String ALIYUN_OSS_DIR_PREFIX;
    @Value("${aliyun.oss.dir.organizationExcelPrefix}")
    public String ALIYUN_OSS_DIR_ORGANIZATIONEXCELPREFIX;

    @Bean
    public OSSClient ossClient() {
        return new OSSClient(ALIYUN_OSS_ENDPOINT, ALIYUN_OSS_ACCESSKEYID, ALIYUN_OSS_ACCESSKEYSECRET);
    }
}
