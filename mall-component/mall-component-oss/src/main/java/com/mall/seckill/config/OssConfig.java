package com.mall.seckill.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

/**
 * @Title: OssConfig
 * @Author Macro Chen
 * @Package com.macro.mall.config
 * @Date 2022/12/21 15:58
 * @description: oss上传配置类
 */
@Data
@Component
@ConfigurationProperties(prefix="sys")
public class OssConfig {

    @NestedConfigurationProperty //指定该属性为嵌套值, 否则默认为简单值导致对象为空（外部类不存在该问题， 内部static需明确指定）
    private Oss oss;

    /** 系统oss配置信息 **/
    @Data
    public static class Oss{

        /** 存储根路径 **/
        private String fileRootPath;

        /** 公共读取块 **/
        private String filePublicPath;

        /** 私有读取块 **/
        private String filePrivatePath;

        /** oss类型 **/
        private String serviceType;

    }
}
