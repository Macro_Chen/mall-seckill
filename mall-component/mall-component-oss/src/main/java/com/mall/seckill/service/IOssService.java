package com.mall.seckill.service;

import com.mall.seckill.domain.OssCallbackResult;
import com.mall.seckill.domain.OssPolicyResult;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * @Title: IOssService
 * @Author Macro Chen
 * @Package com.macro.mall.service
 * @Date 2022/12/21 16:02
 * @description: OSS 服务接口定义
 */
public interface IOssService {

    /**
     * 上传文件 & 生成下载/预览URL
     **/
    Map<String, Object> upload2PreviewUrl(MultipartFile multipartFile) throws IOException;

    /**
     * Oss上传策略生成
     */
    OssPolicyResult policy();
    /**
     * Oss上传成功回调
     */
    OssCallbackResult callback(HttpServletRequest request);

    /**
     * 单文件上传
     * @param in 文件输入流
     * @param fileName 文件名称
     * @return Map
     */
    Map<String, Object> uploadFile(InputStream in, String fileName);
}
