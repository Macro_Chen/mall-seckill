package com.mall.seckill.constant;

import lombok.Getter;

/**
 * @Title: OssServiceTypeEnum
 * @Author Macro Chen
 * @Package com.macro.mall.constant
 * @Date 2022/12/21 16:01
 * @description: oss 服务枚举值
 */
@Getter
public enum OssServiceTypeEnum {

    /**
     * 本地存储
     */
    LOCAL("local"),

    /**
     * 阿里云oss
     */
    ALIYUN_OSS("aliyun-oss");

    /** 名称 **/
    private String serviceName;

    OssServiceTypeEnum(String serviceName){
        this.serviceName = serviceName;
    }
}

