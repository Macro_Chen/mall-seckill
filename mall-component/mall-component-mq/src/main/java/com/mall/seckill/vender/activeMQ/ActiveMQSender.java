package com.mall.seckill.vender.activeMQ;

import com.mall.seckill.constant.MQVenderCS;
import com.mall.seckill.model.AbstractMQ;
import com.mall.seckill.vender.IMQSender;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * @Title: ActiveMQSender
 * @Author Macro Chen
 * @Package com.macro.tiny.vender.activeMQ
 * @Date 2022/12/21 14:11
 * @description: activeMQ 消息发送器
 */
@Component
@ConditionalOnProperty(name = MQVenderCS.YML_VENDER_KEY, havingValue = MQVenderCS.ACTIVE_MQ)
public class ActiveMQSender implements IMQSender {

    @Override
    public void send(AbstractMQ mqModel) {

    }

    @Override
    public void send(AbstractMQ mqModel, int delay) {

    }
}
