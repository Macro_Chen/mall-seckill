package com.mall.seckill.vender;

/**
 * @Title: IMQMsgReceiver
 * @Author Macro Chen
 * @Package com.macro.tiny.vender
 * @Date 2022/12/21 14:03
 * @description: MQ 消息接收器 接口定义
 */
public interface IMQMsgReceiver {

    /** 接收消息 **/
    void receiveMsg(String msg);
}
