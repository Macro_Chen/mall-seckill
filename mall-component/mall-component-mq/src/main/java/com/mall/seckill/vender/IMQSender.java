package com.mall.seckill.vender;

import com.mall.seckill.model.AbstractMQ;

/**
 * @Title: IMQSender
 * @Author Macro Chen
 * @Package com.macro.tiny.vender
 * @Date 2022/12/21 14:03
 * @description: MQ 消息发送器  接口定义
 */
public interface IMQSender {

    /** 推送MQ消息， 实时 **/
    void send(AbstractMQ mqModel);

    /** 推送MQ消息， 延迟接收，单位：s **/
    void send(AbstractMQ mqModel, int delay);

}
