package com.mall.seckill.authorization;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.mall.seckill.config.IgnoreUrlsConfig;
import com.mall.seckill.constant.AuthConstant;
import com.mall.seckill.domain.UserDto;
import com.mall.seckill.handler.TokenResolve;
import com.nimbusds.jose.JWSObject;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.ReactiveAuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 鉴权管理器，用于判断是否有资源的访问权限
 * Created by macro on 2020/6/19.
 */
@Component
@Slf4j
public class AuthorizationManager implements ReactiveAuthorizationManager<AuthorizationContext> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationManager.class);
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private IgnoreUrlsConfig ignoreUrlsConfig;

    @Override
    public Mono<AuthorizationDecision> check(Mono<Authentication> mono, AuthorizationContext authorizationContext) {
        LOGGER.info("======开始进入鉴权管理器========");
        ServerHttpRequest request = authorizationContext.getExchange().getRequest();
        URI uri = request.getURI();
        PathMatcher pathMatcher = new AntPathMatcher();

        //白名单路径直接放行
        List<String> ignoreUrls = ignoreUrlsConfig.getUrls();
        for (String ignoreUrl : ignoreUrls) {
            if (pathMatcher.match(ignoreUrl, uri.getPath())) {
                LOGGER.info("{}为白名单路径", uri.getPath());
                return Mono.just(new AuthorizationDecision(true));
            }
        }


        LOGGER.info("{}不是白名单路径", uri.getPath());
        //对应跨域的预检请求直接放行
        if (request.getMethod() == HttpMethod.OPTIONS) {
            return Mono.just(new AuthorizationDecision(true));
        }
        UserDto userDto;
        //不同用户体系登录不允许互相访问
        try {
            String token = TokenResolve.resolve(request);
            if (StrUtil.isBlank(token)) {
                return Mono.just(new AuthorizationDecision(false));
            }
            String realToken = token.replace(AuthConstant.JWT_TOKEN_PREFIX, "");
            JWSObject jwsObject = JWSObject.parse(realToken);
            String userStr = jwsObject.getPayload().toString();
            userDto = JSONUtil.toBean(userStr, UserDto.class);
            log.info("用户信息:{}===>{}", userStr, userDto);
            if (AuthConstant.ADMIN_CLIENT_ID.equals(userDto.getClientId()) && !pathMatcher.match(AuthConstant.ADMIN_URL_PATTERN, uri.getPath())) {
                return Mono.just(new AuthorizationDecision(false));
            }
            if (AuthConstant.PORTAL_CLIENT_ID.equals(userDto.getClientId()) && pathMatcher.match(AuthConstant.ADMIN_URL_PATTERN, uri.getPath())) {
                return Mono.just(new AuthorizationDecision(false));
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return Mono.just(new AuthorizationDecision(false));
        }

        if (!pathMatcher.match(AuthConstant.ADMIN_URL_PATTERN, uri.getPath())) {
            return Mono.just(new AuthorizationDecision(true));
        }

        // 如果请求的路径为根据授权作用域校验的资源接口根据根据作用域判断权限
        // TODO: 现在只是把macro-admin服务的/admin/**接口更改为根据作用域校验权限的资源 应单独为一个服务
        if (pathMatcher.match(AuthConstant.ADMIN_ADMIN_URL_PATTERN, uri.getPath())) {
            log.info("根据客户端作用域判断是否有权限");
            return getAuthorizationDecisionMonoOfScope(uri, pathMatcher, userDto);
        }
        log.info("根据用户角色判断是否有权限");
        return getAuthorizationDecisionMonoOfRole(mono, uri, pathMatcher);
    }


    /**
     * 根据用户授权的作用域是否包含资源所需的作用域
     */
    private Mono<AuthorizationDecision> getAuthorizationDecisionMonoOfScope(URI uri, PathMatcher pathMatcher, UserDto userDto) {
        List<String> authorities = getAuthorities(AuthConstant.RESOURCE_SCOPES_MAP_KEY, pathMatcher, uri);
        log.info("客户端作用域:{}", userDto.getScope());
        log.info("资源所需要的作用域:{}", authorities);
        for (String scope : userDto.getScope()) {
            if (authorities.contains(scope)) {
                return Mono.just(new AuthorizationDecision(true));
            }
        }
        //认证通过且角色匹配的用户可访问当前路径
        return Mono.just(new AuthorizationDecision(false));
    }

    /**
     * 根据用户所持有角色是否包含资源所需的角色
     */
    private Mono<AuthorizationDecision> getAuthorizationDecisionMonoOfRole(Mono<Authentication> mono, URI uri, PathMatcher pathMatcher) {
        // 获取缓存中所有的资源
        List<String> authorities = getAuthorities(AuthConstant.RESOURCE_ROLES_MAP_KEY, pathMatcher, uri);
        // 转换角色格式
        authorities = authorities.stream().map(i -> i = AuthConstant.AUTHORITY_PREFIX + i).collect(Collectors.toList());
        // 认证通过且角色匹配的用户可访问当前路径
        List<String> finalAuthorities = authorities;

        return mono
                .filter(Authentication::isAuthenticated)
                .flatMapIterable(Authentication::getAuthorities)
                .map(GrantedAuthority::getAuthority)
                .any(role -> {
                            log.info("访问路径:{}", uri.getPath());
                            log.info("用户角色:{}", role);
                            log.info("资源需要的角色:{}", finalAuthorities);
                            return finalAuthorities.contains(role);
                        }
                )
                .map(AuthorizationDecision::new)
                .defaultIfEmpty(new AuthorizationDecision(false));
    }

    /**
     * 获取所有资源所需要的角色或作用域
     *
     * @param resourceKey 缓存key
     * @param pathMatcher 路径校验器
     * @param uri         路径
     * @return 角色列表或作用域列表
     */
    private List<String> getAuthorities(String resourceKey, PathMatcher pathMatcher, URI uri) {
        Map<Object, Object> resourceRolesMap = redisTemplate.opsForHash().entries(resourceKey);
        Iterator<Object> iterator = resourceRolesMap.keySet().iterator();
        List<String> authorities = new ArrayList<>();
        while (iterator.hasNext()) {
            String pattern = (String) iterator.next();
            if (pathMatcher.match(pattern, uri.getPath())) {
                authorities.addAll(Convert.toList(String.class, resourceRolesMap.get(pattern)));
            }
        }
        return authorities;
    }
}
