package com.mall.seckill.handler;

import cn.hutool.core.util.StrUtil;
import com.mall.seckill.constant.AuthConstant;
import org.springframework.http.server.reactive.ServerHttpRequest;

/**
 * @Title: TokenResolve
 * @Author Macro Chen
 * @Package com.mall.seckill.handler
 * @Date 2022/12/31 4:37
 * @description: 加载请求中的token
 */
public class TokenResolve {
    public static String resolve(ServerHttpRequest request) {
        String headerToken = request.getHeaders().getFirst(AuthConstant.JWT_TOKEN_HEADER);
        String paramAccessToken = request.getQueryParams().getFirst(AuthConstant.ACCESS_TOKEN);
        return StrUtil.isNotBlank(headerToken) ? headerToken : paramAccessToken;
    }

}
