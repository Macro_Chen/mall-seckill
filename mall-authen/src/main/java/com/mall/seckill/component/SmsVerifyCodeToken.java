package com.mall.seckill.component;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Title: SmsVerifyCodeToken
 * @Author Macro Chen
 * @Package com.mall.seckill.component
 * @Date 2022/12/25 17:59
 * @description: 短信验证码Token实现类
 */
public class SmsVerifyCodeToken extends AbstractAuthenticationToken {

    /**
     * 用户身份(手机号) Spring Security处理器处理时为手机号
     * 最终交给Oauth2授权时为UserDetails 方便后续AccessToken内容增强
     */
    private final Object principal;

    /**
     * 用户凭证(手机号验证码)
     */
    private final Object credential;


    public SmsVerifyCodeToken(Object principal, Object credential) {
        super(new ArrayList<>());
        this.principal = principal;
        this.credential = credential;
        super.setAuthenticated(false);
    }

    public SmsVerifyCodeToken(Collection<? extends GrantedAuthority> authorities, Object principal, Object credential) {
        super(authorities);
        this.principal = principal;
        this.credential = credential;
        super.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return this.credential;
    }

    @Override
    public Object getPrincipal() {
        return this.principal;
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        if (authenticated) {
            throw new IllegalArgumentException(
                    "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        }
        super.setAuthenticated(false);
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
    }

    @Override
    public String getName() {
        return "SmsVerifyCodeToken";
    }
}
