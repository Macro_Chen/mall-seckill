package com.mall.seckill.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mall.seckill.api.CommonResult;
import com.mall.seckill.api.ResultCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Map;

/**
 * @Title: UnauthorizedEntryPoint
 * @Author Macro Chen
 * @Package com.mall.seckill.component
 * @Date 2023/1/6 19:24
 * @description: 未登录处理
 */
@Component("unauthorizedEntryPoint")
public class UnauthorizedEntryPoint implements AuthenticationEntryPoint {

    /**
     * 默认授权地址
     */
    private static final String AUTHORIZE_URI = "/oauth/authorize";

    /**
     * 远程登录地址
     */
    @Value("${security.login-address}")
    private String loginAddress;


    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        // 获取请求参数
        Map<String, String[]> paramMap = request.getParameterMap();
        StringBuilder param = new StringBuilder();
        paramMap.forEach((k, v) -> {
            param.append("&").append(k).append("=").append(v[0]);
        });
        param.deleteCharAt(0);
        String uri = request.getRequestURI();
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        if (antPathMatcher.match(AUTHORIZE_URI, uri)) {
            // 回调地址为当前请求地址
            String redirectUrl = request.getRequestURL() + "?" + param;
            response.sendRedirect(loginAddress + "?redirect_url=" + URLEncoder.encode(redirectUrl, "UTF-8"));
            return;
        }
        // 返回未登录响应
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        PrintWriter writer = response.getWriter();
        ObjectMapper mapper = new ObjectMapper();
        writer.print(mapper.writeValueAsString(CommonResult.unauthorized(ResultCode.UNAUTHORIZED)));
        writer.flush();
        writer.close();
    }
}
