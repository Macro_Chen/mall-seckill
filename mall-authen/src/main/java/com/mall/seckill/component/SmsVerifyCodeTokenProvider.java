package com.mall.seckill.component;

import com.mall.seckill.constant.NumberConstant;
import com.mall.seckill.constant.RequestParameterConstant;
import com.mall.seckill.domain.SecurityUser;
import com.mall.seckill.domain.UserDto;
import com.mall.seckill.service.RedisService;
import com.mall.seckill.service.UserService;
import com.mall.seckill.utils.AssertUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Map;

/**
 * @Title: SmsVerifyCodeTokenProvider
 * @Author Macro Chen
 * @Package com.mall.seckill.component
 * @Date 2022/12/25 18:08
 * @description: 短信验证码登录处理器
 */
@Slf4j
public class SmsVerifyCodeTokenProvider implements AuthenticationProvider {


    /**
     * 用于校验用户状态以及是否过期
     */
    private final UserDetailsChecker userDetailsChecker = new AccountStatusUserDetailsChecker();

    /**
     * 查询用户信息接口
     */
    private final UserService userService;

    /**
     * 缓存服务
     */
    private final RedisService redisService;


    public SmsVerifyCodeTokenProvider(UserService userService, RedisService redisService) {
        this.userService = userService;
        this.redisService = redisService;
    }

    /**
     * 认证方法
     *
     * @param authentication 认证信息
     * @return Authentication
     * @throws AuthenticationException 认证异常
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        log.info("spring security 认证{}", authentication);
        SmsVerifyCodeToken authenticationToken = (SmsVerifyCodeToken) authentication;
        // 请求参数
        Map<String, String> parameters = (Map<String, String>) authenticationToken.getDetails();
        // 用户手机号
        String phoneNumber = (String) authenticationToken.getPrincipal();
        AssertUtil.isNotEmpty(phoneNumber, "手机号不能为空");
        // 用户登录的短信验证码
        String verifyCode = (String) authenticationToken.getCredentials();
        AssertUtil.isNotEmpty(verifyCode, "验证码不能为空");
        // 根据手机号查询用户信息
        UserDetails userDetails = userService.loadUserByUsername(phoneNumber);
        // 首次短信验证码登录需注册用户信息
        if (userDetails == null) {
            // TODO: 注册首次登录用户
            UserDto userDto = UserDto.builder().status(NumberConstant.ONE)
                    .clientId(parameters.get(RequestParameterConstant.CLIENT_ID))
                    .username(phoneNumber)
                    .roles(Collections.emptyList())
                    .build();
            userDetails = new SecurityUser(userDto);
        }
        userDetailsChecker.check(userDetails);
        SmsVerifyCodeToken smsVerifyCodeToken = new SmsVerifyCodeToken(userDetails.getAuthorities(), userDetails, verifyCode);
        smsVerifyCodeToken.setDetails(authenticationToken);
        return smsVerifyCodeToken;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        // 指定AbstractAuthenticationToken
        return aClass.isAssignableFrom(SmsVerifyCodeToken.class);
    }
}
