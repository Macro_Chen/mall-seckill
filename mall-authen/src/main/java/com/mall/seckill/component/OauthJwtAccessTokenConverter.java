package com.mall.seckill.component;

import com.mall.seckill.service.UserService;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

/**
 * @Title: OauthJwtAccessTokenConverter
 * @Author Macro Chen
 * @Package com.mall.seckill.component
 * @Date 2023/1/2 14:16
 * @description: 自定义jwt token 转换
 */
public class OauthJwtAccessTokenConverter extends JwtAccessTokenConverter {

    public OauthJwtAccessTokenConverter(UserService userService) {
        super.setAccessTokenConverter(new OauthAccessTokenConverter(userService));
    }
}
