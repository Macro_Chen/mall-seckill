package com.mall.seckill.component;


import com.mall.seckill.constant.AuthenticationModeConstant;
import com.mall.seckill.constant.NumberConstant;
import com.mall.seckill.domain.SecurityUser;
import com.mall.seckill.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * JWT内容增强器
 * @Title: SmsCodeTokenGranter
 * @Author Macro Chen
 * @Package com.mall.seckill.component
 * @Date 2022/12/23 16:03
 * @description: JWT内容增强器
 */
@Component
@Slf4j
public class JwtTokenEnhancer implements TokenEnhancer {

    @Autowired
    private UserService userService;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        OAuth2Request oAuth2Request = authentication.getOAuth2Request();
        String currentGranType = oAuth2Request.getGrantType();
        if (!AuthenticationModeConstant.CLIENT_CREDENTIALS.equals(currentGranType))
        {
            SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
            System.out.println("token增强" + authentication.getPrincipal());
            Map<String, Object> info = new HashMap<>(NumberConstant.TWO);
            //把用户ID设置到JWT中
            info.put("id", securityUser.getId());
            info.put("client_id",securityUser.getClientId());
            ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
        }
        return accessToken;
    }
}
