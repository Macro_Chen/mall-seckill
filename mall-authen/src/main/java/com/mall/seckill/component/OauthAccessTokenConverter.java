package com.mall.seckill.component;

import com.mall.seckill.service.UserService;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;

/**
 * @Title: OauthAccessTokenConverter
 * @Author Macro Chen
 * @Package com.mall.seckill.component
 * @Date 2023/1/2 14:15
 * @description: 自定义token转换
 */
public class OauthAccessTokenConverter extends DefaultAccessTokenConverter {

    public OauthAccessTokenConverter(UserService userService) {
        DefaultUserAuthenticationConverter converter = new DefaultUserAuthenticationConverter();
        converter.setUserDetailsService(userService);
        super.setUserTokenConverter(converter);
    }
}
