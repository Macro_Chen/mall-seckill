package com.mall.seckill.domain;

import cn.hutool.core.util.StrUtil;
import com.mall.seckill.utils.JsonUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import java.util.*;

/**
 * @author 陈述智  梦想开始远航的地方
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecurityClient implements ClientDetails {


    private Long id;

    /**
     * 客户端id
     */
    private String clientId;
    /**
     * 客户端名字
     */
    private String clientName;
    /**
     * 客户端密钥  采用BCrypt算法
     */
    private String clientSecret;
    /**
     * 客户端授权类型 这里是多采用的是密码模式
     */
    private List<String> authorizedGrantTypes;

    /**
     * 权限数据 可自定义权限
     */
    private Collection<GrantedAuthority> authorities;

    @ApiModelProperty(value = "客户端授权码模式")
    private String redirectUri;

    @ApiModelProperty(value = "accessToken过期时间")
    private Integer accessTokenValidity;

    @ApiModelProperty(value = "刷新token过期时间")
    private Integer refreshTokenValidity;

    @ApiModelProperty(value = "权限区域")
    private String scope;

    @ApiModelProperty(value = "是否自动授权")
    private String autoApprove;

    @ApiModelProperty(value = "客户端附加说明")
    private String additionalInformation;

    public SecurityClient(ClientDto clientDto) {
        if (Objects.nonNull(clientDto)) {
            BeanUtils.copyProperties(clientDto, this);
            if (clientDto.getAuthorizedGrantTypes() != null) {
                authorities = new ArrayList<>();
                clientDto.getAuthorizedGrantTypes().forEach(item -> authorities.add(new SimpleGrantedAuthority(item)));
            }
        }
    }

    /**
     * @return 返回客户端名字
     */
    @Override
    public String getClientId() {
        return this.clientId;
    }

    /**
     * @return 返回客户端的跳转路径 这里设置为null
     */
    @Override
    public Set<String> getResourceIds() {
        return null;
    }

    /**
     * @return 保密要求
     */
    @Override
    public boolean isSecretRequired() {
        return true;
    }

    /**
     * @return 得到加密秘钥
     */
    @Override
    public String getClientSecret() {
        return this.clientSecret;
    }

    /**
     * @return 是否有范围
     */
    @Override
    public boolean isScoped() {
        return true;
    }

    /**
     * 设置范围
     *
     * @return 返回设置的范围
     */
    @Override
    public Set<String> getScope() {
        Set<String> scopeSet = new HashSet<String>();
        if (StrUtil.isNotBlank(scope)) {
            if (scope.contains(",")) {
                String[] scopes = scope.split(",");
                scopeSet = new HashSet<>(Arrays.asList(scopes));
            }else {
                scopeSet.add(scope);
            }
        }
        return scopeSet;
    }

    /**
     * @return 返回授权类型
     */
    @Override
    public Set<String> getAuthorizedGrantTypes() {
        if (Objects.nonNull(this.authorizedGrantTypes)) {
            return new HashSet<>(this.authorizedGrantTypes);
        }
        return new HashSet<>();
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        Set<String> uriSet = new HashSet<String>();
        if (StrUtil.isNotBlank(redirectUri)) {
            if (redirectUri.contains(",")) {
                String[] uris = redirectUri.split(",");
                uriSet = new HashSet<>(Arrays.asList(uris));
            }else {
                uriSet.add(redirectUri);
            }
        }
        return uriSet;
    }

    /**
     * @return 返回权限列表
     */
    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return this.authorities != null ? this.authorities : new ArrayList<>();
    }

    /**
     * @return 设置token过期时间
     */
    @Override
    public Integer getAccessTokenValiditySeconds() {
        return Objects.nonNull(accessTokenValidity) ? accessTokenValidity : 3600 * 24;
    }

    /**
     * @return 设置刷新token的过期时间
     */
    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return Objects.nonNull(refreshTokenValidity) ? refreshTokenValidity : 3600 * 24 * 7;
    }

    /**
     * 是否自动授权
     */
    @Override
    public boolean isAutoApprove(String s) {
        return "1".equals(autoApprove);
    }

    /**
     * @return 补充资料
     */
    @Override
    public Map<String, Object> getAdditionalInformation() {
        if (StrUtil.isNotBlank(additionalInformation)) {
            return JsonUtils.jsonToPojo(additionalInformation, Map.class);
        }
        return null;
    }
}
