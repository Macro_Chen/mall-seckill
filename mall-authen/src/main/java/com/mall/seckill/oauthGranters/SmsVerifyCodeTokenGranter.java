package com.mall.seckill.oauthGranters;

import com.mall.seckill.component.SmsVerifyCodeToken;
import com.mall.seckill.constant.AuthenticationModeConstant;
import com.mall.seckill.constant.RequestParameterConstant;
import com.mall.seckill.service.UserService;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Title: SmsCodeTokenGranter
 * @Author Macro Chen
 * @Package com.mall.seckill.component
 * @Date 2022/12/23 16:03
 * @description: 短信验证码登录授权模式
 */
@Slf4j
@SuppressWarnings("all")
public class SmsVerifyCodeTokenGranter extends AbstractTokenGranter {


    private final UserService userDetailsService;

    private final AuthenticationManager authenticationManager;

    public SmsVerifyCodeTokenGranter(AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService,
                                     OAuth2RequestFactory requestFactory, UserService userDetailsService,
                                     AuthenticationManager authenticationManager) {
        super(tokenServices, clientDetailsService, requestFactory, AuthenticationModeConstant.SMS_CODE);
        this.userDetailsService = userDetailsService;
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = new LinkedHashMap<String, String>(tokenRequest.getRequestParameters());
        // 客户端提交的手机号码
        String phoneNumber = parameters.get(RequestParameterConstant.LOGIN_PHONE);
        String verifyCode = parameters.get(RequestParameterConstant.LOGIN_VERIFY_CODE);
        SmsVerifyCodeToken userAuth = new SmsVerifyCodeToken(phoneNumber, verifyCode);
        userAuth.setDetails(parameters);
        userAuth = (SmsVerifyCodeToken) authenticationManager.authenticate(userAuth);
        OAuth2Request oAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);

        return new OAuth2Authentication(oAuth2Request, userAuth);
    }
}
