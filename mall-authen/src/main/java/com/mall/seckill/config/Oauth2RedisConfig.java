package com.mall.seckill.config;

import org.springframework.context.annotation.Configuration;

/**
 * @Title: Oauth2RedisConfig
 * @Author Macro Chen
 * @Package com.macro.tiny.config
 * @Date 2022/12/18 16:55
 * @description: redis配置
 */
@Configuration
public class Oauth2RedisConfig extends BaseRedisConfig{
}
