package com.mall.seckill.config;

import com.mall.seckill.component.SmsVerifyCodeTokenProvider;
import com.mall.seckill.component.UnauthorizedEntryPoint;
import com.mall.seckill.service.RedisService;
import com.mall.seckill.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Collections;

/**
 * SpringSecurity配置
 * Created by macro on 2020/6/19.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserService userService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private UnauthorizedEntryPoint unauthorizedEntryPoint;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 增加SMS短信验证码登录处理器 注: 新增的处理器不能注入到IOC当中 否则会覆盖默认表单登录处理器会找不到
        http.authenticationProvider(new SmsVerifyCodeTokenProvider(userService, redisService));
        http.formLogin()
                .and()
                // 认证失败处理
                .exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint)
                .and()
                // Oauth2授权端点地址全部放行
                .authorizeRequests()
                .requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll()
                // 预处理请求放开
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                // rsa公钥获取地址
                .antMatchers("/rsa/publicKey").permitAll()
                // swagger地址
                .antMatchers("/v2/api-docs").permitAll()
                // 授权页面地址
                .antMatchers("/custom/confirm_access").permitAll()
                // 自定义Spring Security登录逻辑地址
                .antMatchers("/login/authentication").permitAll()
                // 其余请求一致需认证
                .anyRequest().authenticated()
                // 关闭csrf校验
                .and()
                .csrf().disable()
                // 跨域配置
                .cors()
                .configurationSource(corsConfigurationSource());
    }


    /**
     * Spring Security 跨域处理
     * @return CorsConfigurationSource
     */
    public CorsConfigurationSource corsConfigurationSource(){
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowedHeaders(Collections.singletonList("*"));
        corsConfiguration.setAllowedMethods(Collections.singletonList("*"));
        corsConfiguration.setAllowedOrigins(Collections.singletonList("*"));
        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.setMaxAge (3600L);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**",corsConfiguration);
        return source;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
