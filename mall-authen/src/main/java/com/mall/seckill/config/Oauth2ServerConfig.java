package com.mall.seckill.config;


import com.mall.seckill.component.JwtTokenEnhancer;
import com.mall.seckill.component.OauthJwtAccessTokenConverter;
import com.mall.seckill.oauthGranters.SmsVerifyCodeTokenGranter;
import com.mall.seckill.service.ClientUserService;
import com.mall.seckill.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author 菲菲
 * 认证服务器配置
 */
@Configuration
@EnableAuthorizationServer
public class Oauth2ServerConfig extends AuthorizationServerConfigurerAdapter {

    @Value("${jwt-convert.credential}")
    private String jwtConvertCredential;
    private final UserService userDetailsService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenEnhancer jwtTokenEnhancer;
    private final ClientUserService clientUserService;

    public Oauth2ServerConfig(UserService userDetailsService, AuthenticationManager authenticationManager, JwtTokenEnhancer jwtTokenEnhancer, ClientUserService clientUserService) {
        this.userDetailsService = userDetailsService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenEnhancer = jwtTokenEnhancer;
        this.clientUserService = clientUserService;
    }

    /**
     * 客户端配置
     * @param clients 客户端
     * @throws Exception Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.withClientDetails(clientUserService);
    }


    /**
     * 端点配置
     * @param endpoints 端点
     * @throws Exception Exception
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        // 令牌增强
        TokenEnhancerChain enhancerChain = new TokenEnhancerChain();
        List<TokenEnhancer> delegates = new ArrayList<>();
        delegates.add(jwtTokenEnhancer);
        delegates.add(accessTokenConverter());
        //配置JWT的内容增强器
        enhancerChain.setTokenEnhancers(delegates);
        endpoints.authenticationManager(authenticationManager)
                //配置加载用户信息的服务
                .userDetailsService(userDetailsService)
                //配置jwt转换器
                .accessTokenConverter(accessTokenConverter())
                //配置令牌增强器
                .tokenEnhancer(enhancerChain)
                //自定义授权模式
                .tokenGranter(tokenGranter(endpoints))
                // 替换自定义授权页面url
                .pathMapping("/oauth/confirm_access", "/custom/confirm_access");
    }

    @Bean
    public TokenStore tokenStore(){
        return new JwtTokenStore(accessTokenConverter());
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
                // 允许通过表单形式获取accessToken
        security.allowFormAuthenticationForClients()
                //  允许通过key形式获取accessToken
                .tokenKeyAccess("permitAll()")
                // 允许校验accessToken
                .checkTokenAccess("permitAll()");
    }


    /**
     * JWT内容转换加密
     * @return JwtAccessTokenConverter
     */
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter jwtAccessTokenConverter = new OauthJwtAccessTokenConverter(userDetailsService);
        jwtAccessTokenConverter.setKeyPair(keyPair());
        return jwtAccessTokenConverter;
    }

    /**
     * 获取秘钥对
     * @return KeyPair
     */
    @Bean
    public KeyPair keyPair() {
        //从classpath下的证书中获取秘钥对
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource("jwt.jks"), jwtConvertCredential.toCharArray());
        return keyStoreKeyFactory.getKeyPair("jwt", jwtConvertCredential.toCharArray());
    }

    /**
     * 添加自定义授权类型
     *
     * @return TokenGranter
     */
    private TokenGranter tokenGranter(AuthorizationServerEndpointsConfigurer endpoints) {

        // endpoints.getTokenGranter() 获取SpringSecurity OAuth2.0 现有的授权类型
        List<TokenGranter> granters = new ArrayList<TokenGranter>(Collections.singletonList(endpoints.getTokenGranter()));
        // 构建短信验证授权类型
        SmsVerifyCodeTokenGranter smsVerifyCodeTokenGranter =
                new SmsVerifyCodeTokenGranter(endpoints.getTokenServices(), endpoints.getClientDetailsService(),
                endpoints.getOAuth2RequestFactory(), userDetailsService, authenticationManager);
        // 向集合中添加短信授权类型
        granters.add(smsVerifyCodeTokenGranter);
        // 返回所有类型
        return new CompositeTokenGranter(granters);
    }

    @Bean
    public ReloadableResourceBundleMessageSource reloadableResource() {
        ReloadableResourceBundleMessageSource reloadable = new ReloadableResourceBundleMessageSource();
        reloadable.setBasename("classpath:org/springframework/security/messages_zh_CN");
        reloadable.setDefaultEncoding("UTF-8");
        return reloadable;
    }

}
