package com.mall.seckill.service.auth;

import com.mall.seckill.domain.Oauth2TokenDto;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.Map;

/**
 * @Title: AuthService
 * @Author Macro Chen
 * @Package com.mall.seckill.service.auth
 * @Date 2022/12/23 23:39
 * @description: 认证服务
 */
public interface AuthService {

    /**
     * 颁发token
     * @param principal 用户凭证
     * @param parameters 参数
     * @return Oauth2TokenDto
     * @throws HttpRequestMethodNotSupportedException 请求方法不允许异常
     */
    Oauth2TokenDto postAccessToken(Principal principal, Map<String, String> parameters) throws HttpRequestMethodNotSupportedException;

    /**
     * 认证获取code
     * @param parameters 参数
     * @param principal 用户凭证(已认证的)
     * @return ModelAndView 返回页面redirect_uri
     */
    ModelAndView authorize(Map<String, Object> model, Map<String, String> parameters, Principal principal);
}
