package com.mall.seckill.service.impl;

import com.mall.seckill.constant.AuthConstant;
import com.mall.seckill.constant.MessageConstant;
import com.mall.seckill.domain.Oauth2TokenDto;
import com.mall.seckill.domain.SecurityUser;
import com.mall.seckill.service.auth.AuthService;
import com.mall.seckill.utils.AssertUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.endpoint.AuthorizationEndpoint;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SimpleSessionStatus;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.Map;
import java.util.Objects;

/**
 *
 */
@Slf4j
@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private TokenEndpoint tokenEndpoint;

    @Autowired
    private AuthorizationEndpoint authorizationEndpoint;

    @Autowired
    private TokenStore tokenStore;

    /**
     * 颁发token
     *
     * @param principal  用户凭证
     * @param parameters 参数
     * @return Oauth2TokenDto
     */
    @Override
    public Oauth2TokenDto postAccessToken(Principal principal, Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
        log.info("用户凭证:{}", principal);
        //实现tokenEndpoint关键类就可以自定义Oauth2的登录认证接口了 重写里面的postAccessToken 直接调用默认的实现逻辑 这样就能更久把结果处理一下就好了
        OAuth2AccessToken oAuth2AccessToken = tokenEndpoint.postAccessToken(principal, parameters).getBody();
        AssertUtil.isTrue(Objects.isNull(oAuth2AccessToken), MessageConstant.GET_TOKEN_FAILED);
        OAuth2Authentication oAuth2Authentication = tokenStore.readAuthentication(oAuth2AccessToken);
        SecurityUser securityUser = (SecurityUser) oAuth2Authentication.getPrincipal();
        Oauth2TokenDto oauth2TokenDto = Oauth2TokenDto.builder()
                .token(oAuth2AccessToken.getValue())
                .refreshToken(Objects.nonNull(oAuth2AccessToken.getRefreshToken())
                        ? oAuth2AccessToken.getRefreshToken().getValue() : "")
                .expiresIn(oAuth2AccessToken.getExpiresIn())
                .tokenHead(AuthConstant.JWT_TOKEN_PREFIX)
                .userId(Objects.nonNull(securityUser) ? securityUser.getId() : 0L).build();
        log.info("oauth2Token信息:{}", oauth2TokenDto);
        return oauth2TokenDto;
    }


    /**
     * 认证获取code
     *
     * @param parameters 参数
     * @param principal  用户凭证(已认证的)
     * @return ModelAndView 返回页面redirect_uri
     */
    @Override
    public ModelAndView authorize(Map<String, Object> model, @RequestParam Map<String, String> parameters, Principal principal) {
        return authorizationEndpoint.authorize(model, parameters, new SimpleSessionStatus(), principal);
    }
}
