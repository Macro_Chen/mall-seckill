package com.mall.seckill.service;


import com.mall.seckill.api.CommonResult;
import com.mall.seckill.api.ResultCode;
import com.mall.seckill.constant.CommonConstant;
import com.mall.seckill.constant.MessageConstant;
import com.mall.seckill.constant.RequestParameterConstant;
import com.mall.seckill.domain.SecurityUser;
import com.mall.seckill.domain.UserDto;
import com.mall.seckill.utils.AssertUtil;
import com.mall.seckill.utils.RequestUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

/**
 * @Title: UserService
 * @Author Macro Chen
 * @Package com.mall.seckill.service
 * @Date 2022/12/23 23:39
 * @description: 用户管理业务类
 */
@Service
@Slf4j
public class UserService implements UserDetailsService {

    @Autowired
    private UmsAdminService adminService;
    @Autowired
    private HttpServletRequest request;

    /**
     * 实现oauth2.0根据用户名查询用户信息接口
     * @param username 用户名
     * @return UserDetails
     * @throws UsernameNotFoundException 用户名或密码错误
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String clientId = getClientIdParameter();
        CommonResult<UserDto> result = adminService.loadUserByUsername(username, CommonConstant.FROM_IN);
        AssertUtil.isTrue(result.getCode() != ResultCode.SUCCESS.getCode(), result.getMessage());
        UserDto userDto = result.getData();
        log.info("远程查询用户信息:{}", userDto);
        try {
            return getUserDetails(clientId, userDto, Boolean.FALSE);
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            e.printStackTrace();
            throw new UsernameNotFoundException(MessageConstant.USERNAME_PASSWORD_ERROR);
        }
    }

    /**
     * 获取客户端名称
     * @return 客户端名称
     */
    private String getClientIdParameter() {
        return RequestUtils.getParameter(request, RequestParameterConstant.CLIENT_ID);
    }


    /**
     *
     * @param clientId 客户端名称
     * @param userDto 用户信息
     * @param notCheckExist 是否不校验用户是否存在
     * @return UserDetails
     */
    private UserDetails getUserDetails(String clientId, UserDto userDto, Boolean notCheckExist) throws InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        // 验证码登录时不存在此用户则返回null 认证处理器会判断然后注册该用户
        if (Objects.isNull(userDto) && notCheckExist) return null;
        // 处理找不到该客户端
        AssertUtil.isNotNull(userDto, UsernameNotFoundException.class, MessageConstant.USERNAME_PASSWORD_ERROR);
        // 处理用户信息
        userDto.setClientId(clientId);
        SecurityUser securityUser = new SecurityUser(userDto);
        // 账号已禁用
        AssertUtil.isTrue(!securityUser.isEnabled(), DisabledException.class, MessageConstant.ACCOUNT_DISABLED);
        // 账号已锁定
        AssertUtil.isTrue(!securityUser.isAccountNonLocked(), LockedException.class, MessageConstant.ACCOUNT_LOCKED);
        // 账号已过期
        AssertUtil.isTrue(!securityUser.isAccountNonExpired(), AccountExpiredException.class, MessageConstant.ACCOUNT_EXPIRED);
        // 登录凭证已过期
        AssertUtil.isTrue(!securityUser.isCredentialsNonExpired(), CredentialsExpiredException.class, MessageConstant.CREDENTIALS_EXPIRED);
        return securityUser;
    }
}
