package com.mall.seckill.service;

import com.mall.seckill.api.CommonResult;
import com.mall.seckill.api.ResultCode;
import com.mall.seckill.constant.CommonConstant;
import com.mall.seckill.domain.ClientDto;
import com.mall.seckill.domain.SecurityClient;
import com.mall.seckill.utils.AssertUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

/**
 * @Title: ClientUserService
 * @Author Macro Chen
 * @Package com.mall.seckill.service
 * @Date 2022/12/23 23:39
 * @description: 客户端服务
 */
@Service
public class ClientUserService implements ClientDetailsService {
    private static final Logger log = LoggerFactory.getLogger(ClientUserService.class);

    @Autowired
    private UmsAdminService umsClientService;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        CommonResult<ClientDto> result = umsClientService.loadClientByClientId(clientId, CommonConstant.FROM_IN);
        log.info("远程查询客户端信息:{}", result);
        AssertUtil.isTrue(result.getCode() != ResultCode.SUCCESS.getCode(), result.getMessage());
        ClientDto clientDto = result.getData();
//        AssertUtil.isNotNull(clientDto, "查询不到该客户端信息");
        return new SecurityClient(clientDto);
    }
}
