package com.mall.seckill.exception;


import com.mall.seckill.api.CommonResult;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局处理Oauth2抛出的异常
 *
 * @author macroChen
 * @date 2020/7/17
 */
@RestControllerAdvice
public class Oauth2ExceptionHandler {

    /**
     * Oauth2抛出的异常
     * @param e OAuth2Exception
     * @return CommonResult
     */
    @ExceptionHandler(value = OAuth2Exception.class)
    public CommonResult<String> handleOauth2(OAuth2Exception e) {
        return CommonResult.failed(e.getMessage());
    }

    /**
     * Oauth2认证异常
     * @param e AuthenticationException
     * @return CommonResult
     */
    @ExceptionHandler(value = AuthenticationException.class)
    public CommonResult<String> handleAuthenticationException(AuthenticationException e) {
        return CommonResult.failed(e.getMessage());
    }


}
