package com.mall.seckill.constant;

/**
 * @Title: AuthenticationModeConstant
 * @Author Macro Chen
 * @Date 2022/12/18 16:55
 * @description: 授权模式常量
 */
public interface AuthenticationModeConstant {

    /**
     * 授权码模式
     */
    String AUTHENTICATION_CODE = "authorization_code";

    /**
     * 密码模式
     */
    String PASSWORD = "password";

    /**
     * 客户端模式
     */
    String CLIENT_CREDENTIALS = "client_credentials";

    /**
     * 验证码模式
     */
    String SMS_CODE = "sms_code";

    /**
     * 简化模式
     */
    String IMPLICIT = "implicit";
}
