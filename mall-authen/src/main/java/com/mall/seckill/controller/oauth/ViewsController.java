package com.mall.seckill.controller.oauth;

import com.mall.seckill.api.CommonResult;
import com.mall.seckill.constant.CommonConstant;
import com.mall.seckill.constant.RequestParameterConstant;
import com.mall.seckill.domain.ClientDto;
import com.mall.seckill.enums.ScopeWithDescriptionEnum;
import com.mall.seckill.service.UmsAdminService;
import com.mall.seckill.utils.ResultUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * 页面跳转Controller
 *
 * @author macroChen
 */
@Controller
@SessionAttributes(RequestParameterConstant.AUTHORIZATION_REQUEST)
public class ViewsController {

    @Autowired
    private UmsAdminService umsAdminService;
//    @Autowired
//    private ApprovalStore approvalStore;

    @ApiOperation(value = "自定义授权页面")
    @RequestMapping("/custom/confirm_access")
    public ModelAndView authorizationPage(Map<String, Object> model, Principal principal, HttpServletRequest request) {
        AuthorizationRequest authorizationRequest = (AuthorizationRequest) model.remove(RequestParameterConstant.AUTHORIZATION_REQUEST);
        // 客户端Id
        String clientId = authorizationRequest.getClientId();
        // 授权范围
        Set<String> scope = authorizationRequest.getScope();
        // 查询客户端信息
        CommonResult<ClientDto> clientResult = umsAdminService.loadClientByClientId(clientId, CommonConstant.FROM_IN);
        ClientDto clientDto = ResultUtils.getData(clientResult, false);
        // 自动授权类别
//        Collection<Approval> approvals = approvalStore.getApprovals(principal.getName(), clientDto.getClientName());
        ModelAndView view = new ModelAndView();
        view.setViewName(CommonConstant.AUTHORIZE_PAGE);
        view.addObject(RequestParameterConstant.ATTRIBUTE_CLIENT_ID, clientId);
        view.addObject(RequestParameterConstant.SCOPES, ScopeWithDescriptionEnum.withDescription(scope));
        view.addObject(RequestParameterConstant.STATE, authorizationRequest.getState());
        view.addObject(RequestParameterConstant.PRINCIPAL_NAME, principal.getName());
        view.addObject(RequestParameterConstant.REDIRECT_URI, authorizationRequest.getRedirectUri());
        view.addObject(RequestParameterConstant.CLIENT_NAME, Objects.nonNull(clientDto) ? clientDto.getClientName() : "未知客户端");
        return view;
    }
}
