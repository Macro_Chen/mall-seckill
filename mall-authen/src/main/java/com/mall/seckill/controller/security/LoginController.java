package com.mall.seckill.controller.security;

import com.mall.seckill.annotation.DecryptPoint;
import com.mall.seckill.api.CommonResult;
import com.mall.seckill.api.ResultCode;
import com.mall.seckill.vo.SecurityUserLoginParams;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Title: LoginController
 * @Author Macro Chen
 * @Package com.mall.seckill.controller.oauth
 * @Date 2023/1/7 18:48
 * @description: 自定义Spring Security登录api
 */
@RestController
@Api(tags = "LoginController", description = "自定义Spring Security登录认证方法 注意区分与Oauth2.0密码模式登录")
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * Spring Security登录认证方法 仅用于Spring Security认证 注意区分与Oauth2的密码模式登录
     * @param params 登录表单参数
     * @return CommonResult
     */
    @PostMapping("/authentication")
    @DecryptPoint
    public CommonResult<String> login(@Validated SecurityUserLoginParams params) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(params.getUsername(), params.getPassword());
        // provideManger认证方法
        Authentication authenticate = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        // 用户凭证添加至上下文 保持登录态
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        return CommonResult.success(ResultCode.SUCCESS.getMessage());
    }
}
