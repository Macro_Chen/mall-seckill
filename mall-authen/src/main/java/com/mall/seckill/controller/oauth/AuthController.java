package com.mall.seckill.controller.oauth;


import com.mall.seckill.api.CommonResult;
import com.mall.seckill.domain.Oauth2TokenDto;
import com.mall.seckill.service.auth.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.endpoint.CheckTokenEndpoint;
import org.springframework.stereotype.Controller;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;
import java.util.Map;

/**
 * 自定义Oauth2获取令牌接口
 *
 * @author macro
 * @date 2020/7/17
 */
@Controller
@Api(tags = "AuthController", description = "认证中心登录认证")
@RequestMapping("/oauth")
public class AuthController {
    @Autowired
    private AuthService authService;
    @Autowired
    private CheckTokenEndpoint checkTokenEndpoint;


    
//    @RequestMapping({"/authorize"})
//    public ModelAndView authorize(Map<String, Object> model, @RequestParam Map<String, String> parameters, Principal principal) {
//        return authService.authorize(model, parameters, principal);
//    }


    @ApiOperation("Oauth2获取token")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "grant_type", value = "授权模式", required = true),
            @ApiImplicitParam(name = "client_id", value = "Oauth2客户端ID", required = true),
            @ApiImplicitParam(name = "client_secret", value = "Oauth2客户端秘钥", required = true),
            @ApiImplicitParam(name = "refresh_token", value = "刷新token"),
            @ApiImplicitParam(name = "username", value = "登录用户名"),
            @ApiImplicitParam(name = "password", value = "登录密码")
    })
    @RequestMapping(value = "/token", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Oauth2TokenDto> postAccessToken(@ApiIgnore Principal principal, @ApiIgnore @RequestParam Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
        return CommonResult.success(authService.postAccessToken(principal, parameters));
    }

    @ApiImplicitParam(name = "token", value = "刷新token", required = true)
    @RequestMapping(value = "/check_token", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Map<String, ?>> checkToken(@RequestParam("token") String token) {
        return CommonResult.success(checkTokenEndpoint.checkToken(token));
    }


}
