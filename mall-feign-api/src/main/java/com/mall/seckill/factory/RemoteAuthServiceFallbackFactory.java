package com.mall.seckill.factory;

import com.mall.seckill.api.CommonResult;
import com.mall.seckill.api.ResultCode;
import com.mall.seckill.constant.ServiceNameConstant;
import com.mall.seckill.service.AuthService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * @Title: RemoteAuthServiceFallbackFactory
 * @Author Macro Chen
 * @Package com.mall.seckill.service.openFeign.factory
 * @Date 2022/12/24 19:44
 * @description: 远程服务AuthService降级处理
 */
@Slf4j
public class RemoteAuthServiceFallbackFactory implements FallbackFactory<AuthService> {

    public RemoteAuthServiceFallbackFactory(){
        log.info("===========测试SpringBoot自动装配加载[RemoteAuthServiceFallbackFactory.class]==========");
    }

    @Override
    public AuthService create(Throwable throwable) {
        log.info("{}服务调用失败====>{}", ServiceNameConstant.MACRO_AUTH, throwable.getMessage());
        return new AuthService() {
            @Override
            public CommonResult<Object> getAccessToken(Map<String, String> parameters, String from) {
                return CommonResult.failed(ResultCode.AUTHENTICATION_FAIL.getMessage());
            }
        };
    }
}
