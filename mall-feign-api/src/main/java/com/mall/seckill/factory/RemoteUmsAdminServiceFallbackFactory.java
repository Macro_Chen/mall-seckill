package com.mall.seckill.factory;

import com.mall.seckill.api.CommonResult;
import com.mall.seckill.api.ResultCode;
import com.mall.seckill.constant.ServiceNameConstant;
import com.mall.seckill.domain.ClientDto;
import com.mall.seckill.domain.UserDto;
import com.mall.seckill.service.UmsAdminService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * @Title: RemoteUmsAdminServiceFallbackFactory
 * @Author Macro Chen
 * @Package com.mall.seckill.factory
 * @Date 2022/12/24 22:22
 * @description: 远程UmsAdminService降级处理
 */
@Slf4j
public class RemoteUmsAdminServiceFallbackFactory implements FallbackFactory<UmsAdminService> {

    @Override
    public UmsAdminService create(Throwable throwable)
    {
        log.info("{}服务调用失败===>{}", ServiceNameConstant.MACRO_ADMIN, throwable.getMessage());

        return new UmsAdminService() {
            @Override
            public CommonResult<UserDto> loadUserByUsername(String username, String from) {
                log.info("加载账号{}信息失败", username);
                return CommonResult.failed(String.format("加载账号%s信息失败", username));
            }

            @Override
            public CommonResult<ClientDto> loadClientByClientId(String clientId, String from) {
                log.info("加载客户端{}信息失败", clientId);
                return CommonResult.failed(ResultCode.CLIENT_INFO_ERROR);
            }
        };
    }
}
