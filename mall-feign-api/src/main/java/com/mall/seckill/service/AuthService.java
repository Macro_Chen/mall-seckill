package com.mall.seckill.service;


import com.mall.seckill.api.CommonResult;
import com.mall.seckill.constant.CommonConstant;
import com.mall.seckill.constant.ServiceNameConstant;
import com.mall.seckill.factory.RemoteAuthServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * 认证服务远程调用
 *
 * @Title: RemoteAuthServiceFallbackFactory
 * @Author Macro Chen
 * @Package com.mall.seckill.service.openFeign.factory
 * @Date 2022/12/24 19:44
 * @description: 认证服务远程调用
 */
@FeignClient(value = ServiceNameConstant.MACRO_AUTH, fallbackFactory = RemoteAuthServiceFallbackFactory.class)
public interface AuthService {

    /**
     * 获取token接口
     * @param parameters 参数
     * @return token信息
     */
    @PostMapping(value = "/oauth/token")
    CommonResult<Object> getAccessToken(@RequestParam Map<String, String> parameters, @RequestHeader(CommonConstant.FROM) String from);

}
