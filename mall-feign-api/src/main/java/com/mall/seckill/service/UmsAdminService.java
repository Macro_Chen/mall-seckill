package com.mall.seckill.service;


import com.mall.seckill.api.CommonResult;
import com.mall.seckill.constant.CommonConstant;
import com.mall.seckill.constant.ServiceNameConstant;
import com.mall.seckill.domain.ClientDto;
import com.mall.seckill.domain.UserDto;
import com.mall.seckill.factory.RemoteUmsAdminServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author macro
 * @date 2019/10/18
 */
@FeignClient(value = ServiceNameConstant.MACRO_ADMIN, fallbackFactory = RemoteUmsAdminServiceFallbackFactory.class)
public interface UmsAdminService {

    /**
     * 远程根据用户名查询用户信息接口
     * @param username 用户名
     * @return UserDto
     */
    @GetMapping("/admin/loadByUsername")
    CommonResult<UserDto> loadUserByUsername(@RequestParam String username, @RequestHeader(CommonConstant.FROM) String from);

    /**
     * 远程根据客户端查询客户端信息接口
     * @param clientId 客户端
     * @return ClientDto
     */
    @RequestMapping("/client/loadClientByClientId")
    CommonResult<ClientDto> loadClientByClientId(@RequestParam String clientId, @RequestHeader(CommonConstant.FROM) String from);
}
